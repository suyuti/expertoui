export const MUSTERI_EKLER      = "Musteri Ekler"
export const MUSTERI_DEGISTIRIR = "Musteri Degistirir"
export const MUSTERI_SILER      = "Musteri Siler"

export const KISI_EKLER      = "Kisi Ekler"
export const KISI_DEGISTIRIR = "Kisi Degistirir"
export const KISI_SILER      = "Kisi Siler"

export const URUN_EKLER      = "Urun Ekler"
export const URUN_DEGISTIRIR = "Urun Degistirir"
export const URUN_SILER      = "Urun Siler"

const permissionMap = {
    "ad" : [],
    "yo" : [],
    'sy' : [
                MUSTERI_EKLER,  MUSTERI_SILER,  MUSTERI_DEGISTIRIR,
                KISI_EKLER,     KISI_SILER,     KISI_DEGISTIRIR,
                URUN_EKLER,     URUN_SILER,     URUN_DEGISTIRIR,
            ],
    "ty" : [],
    "my" : [],
    "sp" : [],
    "tp" : [],
    "mp" : [],
    "mu" : []
}

export  function can (role, action) {
    if (role === 'sa' || role === 'ad' ) return true;
    return permissionMap[role].includes(action)
}

