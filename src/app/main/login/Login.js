import React, {useState} from 'react'
import {Card, CardContent, Typography, Divider, Tabs, Tab} from '@material-ui/core';
import {darken} from '@material-ui/core/styles/colorManipulator';
import {FuseAnimate} from '@fuse';
import {Link} from 'react-router-dom';
import clsx from 'clsx';
import JWTLoginTab from './tabs/JWTLoginTab';
import {makeStyles} from '@material-ui/styles';

const useStyles = makeStyles(theme => ({
    root: {
        background: 'linear-gradient(to right, ' + theme.palette.primary.dark + ' 0%, ' + darken(theme.palette.primary.dark, 0.5) + ' 100%)',
        color     : theme.palette.primary.contrastText
    }
}));

function Login()
{
    const classes = useStyles();
    const [selectedTab, setSelectedTab] = useState(0);

    function handleTabChange(event, value)
    {
        setSelectedTab(value);
    }

    return (
        <div className={clsx(classes.root, "flex flex-col flex-1 flex-shrink-0 p-24 md:flex-row md:p-0")}>

            <div className="flex flex-col flex-grow-0 items-center text-white p-16 text-center md:p-128 md:items-start md:flex-shrink-0 md:flex-1 md:text-left">

                <FuseAnimate animation="transition.expandIn">
                <img className="w-128 mb-32" src="assets/images/logos/experto-logo.png" alt="logo" />
                </FuseAnimate>

                <FuseAnimate animation="transition.slideUpIn" delay={300}>
                <Typography variant="h3" color="inherit" className="font-light">
                        EXPERTO
                    </Typography>
                </FuseAnimate>
                
                <FuseAnimate animation="transition.slideUpIn" delay={300}>
                    <Typography variant="body" color="inherit" className="font-light">
                        {process.env.REACT_APP_TYPE}
                    </Typography>
                </FuseAnimate>

                <FuseAnimate delay={400}>
                    <Typography variant="subtitle1" color="inherit" className="max-w-512 mt-16">
                    </Typography>
                </FuseAnimate>

                {(process.env.REACT_APP_TYPE === 'dev' ||
                process.env.REACT_APP_TYPE === 'test') &&
                <FuseAnimate delay={400}>
                <div className="flex flex-col items-center pt-24">
                <Typography className="text-14 font-600 py-8">
                    Credentials
                </Typography>

                <Divider className="mb-16 w-600"/>

                <table className="text-left border">
                    <thead className= "border">
                        <tr>
                            <th><Typography className="mx-36" >Role</Typography></th>
                            <th><Typography className="mx-36" >Username</Typography></th>
                            <th><Typography className="mx-36" >Password</Typography></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><Typography className="font-600 mx-36" >SuperAdmin</Typography></td>
                            <td><Typography className="font-600 mx-36" >admin@experto.com.tr</Typography></td>
                            <td><Typography className="font-600 mx-36" >123456</Typography></td>
                        </tr>
                        <tr>
                            <td><Typography className="font-600 mx-36" >Admin</Typography></td>
                            <td><Typography className="font-600 mx-36" >admin@experto.com.tr</Typography></td>
                            <td><Typography className="font-600 mx-36" >123456</Typography></td>
                        </tr>
                        <tr>
                            <td><Typography className="font-600 mx-36" >Yonetici</Typography></td>
                            <td><Typography className="font-600 mx-36" >yo@experto.com.tr</Typography></td>
                            <td><Typography className="font-600 mx-36" >123456</Typography></td>
                        </tr>
                        <tr>
                            <td><Typography className="font-600 mx-36" >Satis Yonetici</Typography></td>
                            <td><Typography className="font-600 mx-36" >sy1@experto.com.tr</Typography></td>
                            <td><Typography className="font-600 mx-36" >123456</Typography></td>
                        </tr>
                        <tr>
                            <td><Typography className="font-600 mx-36" >Teknik Yonetici</Typography></td>
                            <td><Typography className="font-600 mx-36" >ty1@experto.com.tr</Typography></td>
                            <td><Typography className="font-600 mx-36" >123456</Typography></td>
                        </tr>
                        <tr>
                            <td><Typography className="font-600 mx-36" >Mali Yonetici</Typography></td>
                            <td><Typography className="font-600 mx-36" >my1@experto.com.tr</Typography></td>
                            <td><Typography className="font-600 mx-36" >123456</Typography></td>
                        </tr>
                        <tr>
                            <td><Typography className="font-600 mx-36" >Satis Personel</Typography></td>
                            <td><Typography className="font-600 mx-36" >sp1@experto.com.tr</Typography></td>
                            <td><Typography className="font-600 mx-36" >123456</Typography></td>
                        </tr>
                        <tr>
                            <td><Typography className="font-600 mx-36" >Teknik Personel</Typography></td>
                            <td><Typography className="font-600 mx-36" >tp1@experto.com.tr</Typography></td>
                            <td><Typography className="font-600 mx-36" >123456</Typography></td>
                        </tr>
                        <tr>
                            <td><Typography className="font-600 mx-36" >Mali Personel</Typography></td>
                            <td><Typography className="font-600 mx-36" >mp1@experto.com.tr</Typography></td>
                            <td><Typography className="font-600 mx-36" >123456</Typography></td>
                        </tr>
                        <tr>
                            <td><Typography className="font-600 mx-36" >Musteri</Typography></td>
                            <td><Typography className="font-600 mx-36" >m1@example.com</Typography></td>
                            <td><Typography className="font-600 mx-36" ></Typography></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            </FuseAnimate>
            }


            </div>

            <FuseAnimate animation={{translateX: [0, '100%']}}>

                <Card className="w-full max-w-400 mx-auto m-16 md:m-0" square>

                    <CardContent className="flex flex-col items-center justify-center p-32 md:p-48 md:pt-128 ">

                        <Typography variant="h6" className="text-center md:w-full mb-48">Giriş</Typography>

                        <Tabs
                            value={selectedTab}
                            onChange={handleTabChange}
                            variant="fullWidth"
                            className="w-full mb-32"
                        >
                            <Tab
                                icon={<img className="h-40 p-4 bg-black rounded-12" src="assets/images/logos/jwt.svg" alt="firebase"/>}
                                className="min-w-0"
                                label="EXPERTO"
                            />
                        </Tabs>

                        {selectedTab === 0 && <JWTLoginTab/>}

                        <div className="flex flex-col items-center justify-center pt-32">
                            <span className="font-medium">Hesabınız yok mu?</span>
                            <Link className="font-medium" to="/register">Hesap oluştur</Link>
                            <Link className="font-medium mt-8" to="/forgot">Şifremi Unuttum</Link>
                        </div>

                    </CardContent>
                </Card>
            </FuseAnimate>
        </div>
    )
}

export default Login;
