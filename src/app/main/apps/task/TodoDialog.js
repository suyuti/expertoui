import React, {useCallback, useEffect, useState} from 'react';
import {
    TextField,
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    FormControl,
    Chip,
    Icon,
    IconButton,
    Typography,
    Toolbar,
    AppBar,
    Avatar,
    Checkbox,
    Menu,
    MenuItem,
    ListItemIcon,
    ListItemText,
    Divider
} from '@material-ui/core';
import {amber, red} from '@material-ui/core/colors';
import {FuseUtils} from '@fuse';
import {useForm} from '@fuse/hooks';
import {useDispatch, useSelector} from 'react-redux';
import moment from 'moment/moment';
import _ from '@lodash';
import * as Actions from './store/actions';

const defaultFormState = {
    '_id'       : '',
    'Baslik'    : '',
    'Aciklama'    : '',
    'StartDate': new Date(),
    'DueDate'  : new Date(),
    'Completed': false,
    'Starred'  : false,
    'Important': false,
    'Deleted'  : false,
    'Labels'   : []
};

function TodoDialog(props)
{
    const dispatch = useDispatch();
    const todoDialog = useSelector(({todoApp}) => todoApp.todos.todoDialog);
    const labels = useSelector(({todoApp}) => todoApp.labels);

    const [labelMenuEl, setLabelMenuEl] = useState(null);
    const {form, handleChange, setForm} = useForm({...defaultFormState});
    const startDate = moment(form.startDate).format(moment.HTML5_FMT.DATETIME_LOCAL_SECONDS);
    const dueDate = moment(form.dueDate).format(moment.HTML5_FMT.DATETIME_LOCAL_SECONDS);

    const initDialog = useCallback(
        () => {
            /**
             * Dialog type: 'edit'
             */
            if ( todoDialog.type === 'edit' && todoDialog.data )
            {
                console.log(todoDialog.data)
                setForm({...todoDialog.data});
            }

            /**
             * Dialog type: 'new'
             */
            if ( todoDialog.type === 'new' )
            {
                setForm({
                    ...defaultFormState,
                    ...todoDialog.data,
                    id: FuseUtils.generateGUID()
                });
            }
        },
        [todoDialog.data, todoDialog.type, setForm]
    );

    useEffect(() => {
        /**
         * After Dialog Open
         */
        if ( todoDialog.props.open )
        {
            initDialog();
        }

    }, [todoDialog.props.open, initDialog]);

    function closeTodoDialog()
    {
        todoDialog.type === 'edit' ? dispatch(Actions.closeEditTodoDialog()) : dispatch(Actions.closeNewTodoDialog());
    }

    function handleLabelMenuOpen(event)
    {
        setLabelMenuEl(event.currentTarget);
    }

    function handleLabelMenuClose(event)
    {
        setLabelMenuEl(null);
    }

    function handleToggleImportant()
    {
        setForm({
            ...form,
            Important: !form.Important
        });
    }

    function handleToggleStarred()
    {
        setForm({
            ...form,
            Starred: !form.Starred
        });
    }

    function handleToggleLabel(event, id)
    {
        event.stopPropagation();
        setForm(
            _.set({
                ...form,
                Labels: form.Labels.includes(id) ? form.Labels.filter(labelId => labelId !== id) : [...form.Labels, id]
            })
        );
    }

    function toggleCompleted()
    {
        setForm({
            ...form,
            Completed: !form.Completed
        })
    }

    function canBeSubmitted()
    {
        return (
            form.Baslik.length > 0
        );
    }

    console.log(form)

    return (
        <Dialog {...todoDialog.props} onClose={closeTodoDialog} fullWidth maxWidth="sm">

            <AppBar position="static" elevation={1}>
                <Toolbar className="flex w-full">
                    <Typography variant="subtitle1" color="inherit">
                        {todoDialog.type === 'new' ? 'Yeni Task' : 'Task Güncelleme'}
                    </Typography>
                </Toolbar>
            </AppBar>

            <DialogContent classes={{root: "p-0"}}>

                <div className="mb-16">
                    <div className="flex items-center justify-between p-12">

                        <div className="flex">
                            <Checkbox
                                tabIndex={-1}
                                checked={form.Completed}
                                onChange={toggleCompleted}
                                onClick={(ev) => ev.stopPropagation()}
                            />
                        </div>

                        <div className="flex items-center justify-start" aria-label="Toggle star">
                            <IconButton onClick={handleToggleImportant}>
                                {form.Important ? (
                                    <Icon style={{color: red[500]}}>error</Icon>
                                ) : (
                                    <Icon>error_outline</Icon>
                                )}
                            </IconButton>

                            <IconButton onClick={handleToggleStarred}>
                                {form.Starred ? (
                                    <Icon style={{color: amber[500]}}>star</Icon>
                                ) : (
                                    <Icon>star_outline</Icon>
                                )}
                            </IconButton>
                            <div>
                                <IconButton
                                    aria-owns={labelMenuEl ? 'label-menu' : null}
                                    aria-haspopup="true"
                                    onClick={handleLabelMenuOpen}
                                >
                                    <Icon>label</Icon>
                                </IconButton>
                                <Menu
                                    id="label-menu"
                                    anchorEl={labelMenuEl}
                                    open={Boolean(labelMenuEl)}
                                    onClose={handleLabelMenuClose}
                                >
                                    {labels.length > 0 && labels.map((label) => {
                                        console.log(label)
                                        console.log(form)
                                        return (
                                        <MenuItem onClick={(ev) => handleToggleLabel(ev, label._id)} key={label._id}>
                                            <ListItemIcon className="min-w-24">
                                                <Icon  color="action">
                                                    {form.Labels.includes(label._id) ? 'check_box' : 'check_box_outline_blank'}
                                                </Icon>
                                            </ListItemIcon>
                                            <ListItemText className="mx-8" primary={label.Title} disableTypography={true}/>
                                            <ListItemIcon className="min-w-24">
                                                <Icon style={{color: label.Color}} color="action">
                                                    label
                                                </Icon>
                                            </ListItemIcon>
                                        </MenuItem>
                                    )})}
                                </Menu>
                            </div>
                        </div>
                    </div>
                    <Divider className="mx-24"/>
                </div>

                {form.Labels.length > 0 && (
                    <div className="flex flex-wrap w-full px-12 sm:px-20 mb-16">
                        {form.Labels.map(label => {
                            console.log(_.find(labels, {_id: label}))
                            return (


                            <Chip
                                avatar={(
                                    <Avatar
                                        classes={{colorDefault: "bg-transparent"}}>
                                        <Icon
                                            className="text-20"
                                            style={{color: _.find(labels, {_id: label}).color}}
                                        >
                                            label
                                        </Icon>
                                    </Avatar>
                                )}
                                label={_.find(labels, {_id: label}).Title}
                                onDelete={(ev) => handleToggleLabel(ev, label)}
                                className="mx-4 my-4"
                                classes={{label: "px-8"}}
                                key={label}
                            />
                        )})}
                    </div>
                )}

                <div className="px-16 sm:px-24">
                    <FormControl className="mt-8 mb-16" required fullWidth>
                        <TextField
                            label="Başlık"
                            autoFocus
                            name="Baslik"
                            value={form.Baslik}
                            onChange={handleChange}
                            required
                            variant="outlined"
                        />
                    </FormControl>

                    <FormControl className="mt-8 mb-16" required fullWidth>
                        <TextField
                            label="Açıklama"
                            name="Aciklama"
                            multiline
                            rows="6"
                            value={form.Aciklama}
                            onChange={handleChange}
                            variant="outlined"
                        />
                    </FormControl>
                    <div className="flex -mx-4">
                        <TextField
                            name="startDate"
                            label="Başlangıç tarihi"
                            type="datetime-local"
                            className="mt-8 mb-16 mx-4"
                            InputLabelProps={{
                                shrink: true
                            }}
                            inputProps={{
                                max: dueDate
                            }}
                            value={startDate}
                            onChange={handleChange}
                            variant="outlined"
                        />
                        <TextField
                            name="dueDate"
                            label="Bitiş tarihi"
                            type="datetime-local"
                            className="mt-8 mb-16 mx-4"
                            InputLabelProps={{
                                shrink: true
                            }}
                            inputProps={{
                                min: startDate
                            }}
                            value={dueDate}
                            onChange={handleChange}
                            variant="outlined"
                        />
                    </div>
                </div>

            </DialogContent>

            {todoDialog.type === 'new' ? (
                <DialogActions className="justify-between p-8">
                    <div className="px-16">
                        <Button
                            variant="contained"
                            color="primary"
                            onClick={() => {
                                dispatch(Actions.addTodo(form));
                                closeTodoDialog();
                            }}
                            disabled={!canBeSubmitted()}
                        >
                            Oluştur
                        </Button>
                    </div>
                </DialogActions>
            ) : (
                <DialogActions className="justify-between p-8">
                    <div className="px-16">
                        <Button
                            variant="contained"
                            color="primary"
                            onClick={() => {
                                dispatch(Actions.updateTodo(form));
                                closeTodoDialog();
                            }}
                            disabled={!canBeSubmitted()}
                        >
                            Kaydet
                        </Button>
                    </div>
                    <IconButton
                        className="min-w-auto"
                        onClick={() => {
                            dispatch(Actions.removeTodo(form._id));
                            closeTodoDialog();
                        }}
                    >
                        <Icon>delete</Icon>
                    </IconButton>
                </DialogActions>
            )}
        </Dialog>
    );
}

export default TodoDialog;
