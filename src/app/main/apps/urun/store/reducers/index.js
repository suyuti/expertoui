import {combineReducers} from 'redux';
import urunler from './urunler.reducer';
import urun from './urun.reducer';

const reducer = combineReducers({
    urun,
    urunler
});

export default reducer;
