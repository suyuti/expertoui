import * as Actions from '../actions';

const initialState = {
    data: null,
    showYeniUrunDlg: false
};

const urunReducer = function (state = initialState, action) {
    switch (action.type) {
        case Actions.GET_URUN:
            {
                return {
                    ...state,
                    data: action.urun
                };
            }
            case Actions.OPEN_YENI_URUN_DLG: {
                return {
                    ...state,
                    showYeniUrunDlg: true
                }
            }
            case Actions.CLOSE_YENI_URUN_DLG: {
                return {
                    ...state,
                    showYeniUrunDlg: false
                }
            }
            case Actions.UPDATE_URUN:
                {
                return {
                    ...state,
                    data: action.urun
                }    
                }
                default:
            {
                return state;
            }
    }
};

export default urunReducer;
