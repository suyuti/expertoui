import * as Actions from '../actions';

const initialState = {
    urunler: [],
    searchText: ''
};

const urunlerReducer = function (state = initialState, action) {
    switch (action.type) {
        case Actions.GET_URUN_LIST:
            {
                return {
                    ...state,
                    urunler: action.urunler
                };
            }
        case Actions.SET_URUNLER_SEARCH_TEXT:
            {
                return {
                    ...state,
                    searchText: action.searchText
                };
            }
        default:
            {
                return state;
            }
    }
};

export default urunlerReducer;
