import { http } from 'app/services/axiosService/axios'
import {getUrunList} from './urunler.actions'

export const GET_URUN = '[URUN APP] GET URUN';
export const UPDATE_URUN = '[URUN APP] UPDATE URUN';
export const ADD_URUN = '[URUN APP] ADD URUN';
export const REMOVE_URUN = '[URUN APP] REMOVE URUN';
export const OPEN_YENI_URUN_DLG = '[URUN APP] OPEN YENI URUN DLG';
export const CLOSE_YENI_URUN_DLG = '[URUN APP] CLOSE YENI URUN DLG';

export function getUrun(id) {
    const request = http.get(`/api/urun/${id}`);

    return (dispatch) =>
        request.then((response) => {
            dispatch({
                type: GET_URUN,
                urun: response
            })
        }
    );
}

export function addUrun(urun) {
    const request = http.post(`/api/urun`, urun);

    return (dispatch) =>
        request.then((response) => {
            dispatch(getUrunList())
            dispatch({
                type: ADD_URUN,
                urun: response
            })
        }
    );
}

export function updateUrun(urun) {
    const request = http.put(`/api/urun/${urun._id}`, urun);

    return (dispatch) =>
        request.then((response) => {
            dispatch({
                type: UPDATE_URUN,
                urun: response
            })
        }
    );
}

export function removeUrun(id) {
    const request = http.delete(`/api/urun/${id}`);

    return (dispatch) =>
        request.then((response) => {
            dispatch(getUrunList())
            dispatch({
                type: REMOVE_URUN,
                urun: response
            })
        }
    );
}

export function openYeniUrunDlg() {
    return {
        type: OPEN_YENI_URUN_DLG
    }
}

export function closeYeniUrunDlg() {
    return {
        type: CLOSE_YENI_URUN_DLG
    }
}