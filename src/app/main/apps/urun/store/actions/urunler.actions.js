import { http } from 'app/services/axiosService/axios'

export const GET_URUN_LIST = '[URUN APP] GET URUN LIST';
export const SET_URUNLER_SEARCH_TEXT = '[URUN APP] SET URUNLER SEARCH TEXT';

export function getUrunList() {
    const request = http.get('/api/urun');

    return (dispatch) =>
        request.then((response) => {
            dispatch({
                type: GET_URUN_LIST,
                urunler: response
            })
        }
    );
}

export function setUrunlerSearchText(event)
{
    return {
        type      : SET_URUNLER_SEARCH_TEXT,
        searchText: event.target.value
    }
}
