import React from 'react'
import {FusePageCarded} from '@fuse';
import UrunlerHeader from './UrunlerHeader'
import UrunlerTable from './UrunlerTable'
import reducer from '../store/reducers';
import withReducer from 'app/store/withReducer';
import YeniUrunDlg from '../urun/YeniUrunDlg'

function Urunler(props) {
    return (
        <React.Fragment>
        <FusePageCarded
            classes={{
                content: "flex",
                header : "min-h-72 h-72 sm:h-136 sm:min-h-136"
            }}
            header={
                <UrunlerHeader/>
            }
            content={
                <UrunlerTable/>
            }
            innerScroll
        />
        <YeniUrunDlg />
        </React.Fragment>
    )
}

export default withReducer('urunApp', reducer)(Urunler);
