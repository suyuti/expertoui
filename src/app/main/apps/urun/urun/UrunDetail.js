import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import withReducer from 'app/store/withReducer';
import { FusePageCarded, FuseLoading, FuseAnimate } from '@fuse';
import * as Actions from '../store/actions';
import reducer from '../store/reducers';
import { makeStyles, useTheme } from '@material-ui/styles';
import { Checkbox, Button, Tab, Tabs, TextField, InputAdornment, Icon, Typography, Select, MenuItem, InputLabel, FormControl, Divider } from '@material-ui/core';
import { Link } from 'react-router-dom';
import { useForm } from '@fuse/hooks';


function UrunDetail(props) {
    const dispatch = useDispatch();
    const urun = useSelector(({ urunApp }) => urunApp.urun);
    const theme = useTheme();
    const { form, handleChange, setForm } = useForm(null);

    function TutarInputHandler(e) {
        let val = e.target.value.length == 0 ? 0 : e.target.value
        let tutar = parseInt(val)
        if (!isNaN(tutar)) {
            handleChange(e)
        }
    }

    useEffect(() => {
        function getUrun() {
            dispatch(Actions.getUrun(props.match.params.urunId))
        }
        getUrun()
    }, [dispatch, props.match.params])

    useEffect(() => {
        if ((urun.data && !form) ||
            (urun.data && form && urun.data._id !== form._id)) {
            setForm(urun.data)
        }
    }, [urun.data, form, setForm])

    if (!urun.data) {
        return <FuseLoading />
    }
    if (!form) {
        return <FuseLoading />
    }

    if (urun.data._id !== props.match.params.urunId) {
        return <FuseLoading />
    }

    return (
        <FusePageCarded
            classes={{
                toolbar: "p-0",
                header: "min-h-72 h-72 sm:h-136 sm:min-h-136"
            }}
            header={(
                <div className="flex flex-1 w-full items-center justify-between">
                    <div className="flex flex-col items-start max-w-full">
                        <FuseAnimate animation="transition.slideRightIn" delay={300}>
                            <Typography
                                className="normal-case flex items-center sm:mb-12"
                                component={Link}
                                role="button"
                                to="/apps/urunler"
                                color="inherit">
                                <Icon className="text-20">{theme.direction === "ltr" ? "arrow_back" : "arrow_forward"}</Icon>
                                <span className="mx-4">Ürünler</span>
                            </Typography>
                        </FuseAnimate>
                        <div className="flex items-center max-w-full">
                            <div className="flex flex-col min-w-0 mx-8 sm:mc-16">
                                <FuseAnimate animation="transition.slideLeftIn" delay={300}>
                                    <Typography className="text-16 sm:text-20 truncate">
                                        {urun.data.Adi}
                                    </Typography>
                                </FuseAnimate>
                                <FuseAnimate animation="transition.slideLeftIn" delay={300}>
                                    <Typography variant="caption">Ürün Detay</Typography>
                                </FuseAnimate>
                            </div>
                        </div>
                    </div>
                    <FuseAnimate animation="transition.slideRightIn" delay={300}>
                        <Button
                            className="whitespace-no-wrap normal-case"
                            variant="contained"
                            color="secondary"
                            //disabled={!canBeSubmitted()}
                            onClick={() => {
                                dispatch(Actions.updateUrun(form))
                                props.history.goBack()
                            }}
                        >
                            Kaydet
                            </Button>
                    </FuseAnimate>

                </div>
            )}
            content={(
                <div className="p-16 sm:p-24 max-w-2xl">
                    <div className="flex">
                        <div className="min-w-48 pt-20">
                            <Icon color="action">account_circle</Icon>
                        </div>

                        <TextField
                            className="mb-24"
                            label="Ürün Adı"
                            autoFocus
                            id="Adi"
                            name="Adi"
                            value={form.Adi}
                            onChange={handleChange}
                            variant="outlined"
                            required
                            fullWidth
                        />
                    </div>
                    <div className="flex">
                        <div className="min-w-48 pt-20">
                            <Icon color="action">account_circle</Icon>
                        </div>
                        <Checkbox
                            className="mb-24"
                            checked={form.OnOdemeTutariVar}
                            name="OnOdemeTutariVar"
                            onChange={handleChange}
                            tabIndex={-1}
                            disableRipple
                            size="medium"
                        />
                        <TextField
                            className="mb-24"
                            label="Ön Ödeme Tutarı"
                            id="OnOdemeTutari"
                            name="OnOdemeTutari"
                            value={form.OnOdemeTutariVar ? form.OnOdemeTutari : ''}
                            onChange={TutarInputHandler}
                            type="text"
                            variant="outlined"
                            fullWidth
                            disabled={!form.OnOdemeTutariVar}
                            InputProps={{
                                startAdornment: <InputAdornment position="start">TL</InputAdornment>
                            }}
                        />
                    </div>
                    <div className="flex">
                        <div className="min-w-48 pt-20">
                            <Icon color="action">account_circle</Icon>
                        </div>
                        <Checkbox
                            className="mb-24"
                            checked={form.SabitTutarVar}
                            name="SabitTutarVar"
                            onChange={handleChange}
                            tabIndex={-1}
                            disableRipple
                            size="medium"
                        />
                        <TextField
                            className="mb-24"
                            label="Sabit Tutar"
                            id="SabitTutar"
                            name="SabitTutar"
                            value={form.SabitTutarVar ? form.SabitTutar : ''}
                            onChange={TutarInputHandler}
                            type="text"
                            variant="outlined"
                            fullWidth
                            disabled={!form.SabitTutarVar}
                            InputProps={{
                                startAdornment: <InputAdornment position="start">TL</InputAdornment>
                            }}
                        />
                    </div>
                    <div className="flex">
                        <div className="min-w-48 pt-20">
                            <Icon color="action">account_circle</Icon>
                        </div>
                        <Checkbox
                            className="mb-24"
                            checked={form.RaporBasiOdemeTutariVar}
                            name="RaporBasiOdemeTutariVar"
                            onChange={handleChange}
                            tabIndex={-1}
                            disableRipple
                            size="medium"
                        />
                        <TextField
                            className="mb-24"
                            label="Rapor Başı Ödeme Tutarı"
                            id="RaporBasiOdemeTutari"
                            name="RaporBasiOdemeTutari"
                            value={form.RaporBasiOdemeTutariVar ? form.RaporBasiOdemeTutari : ''}
                            onChange={TutarInputHandler}
                            type="text"
                            variant="outlined"
                            fullWidth
                            disabled={!form.RaporBasiOdemeTutariVar}
                            InputProps={{
                                startAdornment: <InputAdornment position="start">TL</InputAdornment>
                            }}
                        />
                    </div>
                    <div className="flex">
                        <div className="min-w-48 pt-20">
                            <Icon color="action">account_circle</Icon>
                        </div>
                        <Checkbox
                            className="mb-24"
                            checked={form.YuzdeTutariVar}
                            name="YuzdeTutariVar"
                            onChange={handleChange}
                            tabIndex={-1}
                            disableRipple
                            size="medium"
                        />
                        <TextField
                            className="mb-24"
                            label="Yüzde Tutarı"
                            id="YuzdeTutari"
                            name="YuzdeTutari"
                            value={form.YuzdeTutariVar? form.YuzdeTutari: ''}
                            onChange={TutarInputHandler}
                            type="text"
                            variant="outlined"
                            fullWidth
                            disabled={!form.YuzdeTutariVar}
                            InputProps={{
                                startAdornment: <InputAdornment position="start">TL</InputAdornment>
                            }}
                        />
                    </div>
                    <div className="flex">
                        <div className="min-w-48 pt-20">
                            <Icon color="action">account_circle</Icon>
                        </div>
                        <TextField
                            className="mb-24"
                            label="KDV Oranı"
                            id="KDVOrani"
                            name="KDVOrani"
                            value={form.KDVOrani}
                            onChange={TutarInputHandler}
                            type="text"
                            variant="outlined"
                            fullWidth
                            InputProps={{
                                startAdornment: <InputAdornment position="start">%</InputAdornment>
                            }}
                        />
                    </div>
                </div>
            )}
            innerScroll
        />
    )
}

export default withReducer('urunApp', reducer)(UrunDetail);
