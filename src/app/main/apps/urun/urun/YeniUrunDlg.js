import React, { useState } from 'react'
import { Checkbox, TextField, Button, Dialog, DialogActions, DialogContent, Icon, IconButton, Typography, Toolbar, AppBar, Avatar, InputAdornment } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import { useForm } from '@fuse/hooks';
import * as Actions from '../store/actions';

function YeniUrunDlg(props) {
    const dispatch = useDispatch();
    const showUrunDlg = useSelector(({ urunApp }) => urunApp.urun.showYeniUrunDlg);
    const { form, handleChange, setForm } = useForm({});

    function handleSubmit(e) {
        e.preventDefault()
        dispatch(Actions.addUrun(form))
        dispatch(Actions.closeYeniUrunDlg())
        //props.history.goBack()
    }

    function TutarInputHandler(e) {
        let val = e.target.value.length == 0 ? 0 : e.target.value
        let tutar = parseInt(val)
        if (!isNaN(tutar)) {
            handleChange(e)
        }
    }


    return (
        <Dialog
            classes={{
                paper: "m-24"
            }}
            open={showUrunDlg}
            fullWidth
            maxWidth="xs"
        >
            <AppBar position="static" elevation={1}>
                <Toolbar className="flex w-full">
                    <Typography variant="subtitle1" color="inherit">
                        Yeni Ürün
                </Typography>
                </Toolbar>
                <div className="flex flex-col items-center justify-center pb-24">
                </div>
            </AppBar>
            <form noValidate
                //onSubmit={handleSubmit} 
                className="flex flex-col md:overflow-hidden">
                <DialogContent classes={{ root: "p-24" }}>

                    <div className="flex">
                        <div className="min-w-48 pt-20">
                            <Icon color="action">account_circle</Icon>
                        </div>

                        <TextField
                            className="mb-24"
                            label="Ürün Adı"
                            autoFocus
                            id="Adi"
                            name="Adi"
                            value={form.Adi}
                            onChange={handleChange}
                            variant="outlined"
                            required
                            fullWidth
                        />
                    </div>
                    <div className="flex">
                        <div className="min-w-48 pt-20">
                            <Icon color="action">account_circle</Icon>
                        </div>
                        <Checkbox
                            className="mb-24"
                            checked={form.OnOdemeTutariVar}
                            name="OnOdemeTutariVar"
                            onChange={handleChange}
                            tabIndex={-1}
                            disableRipple
                            size="medium"
                        />
                        <TextField
                            className="mb-24"
                            label="Ön Ödeme Tutarı"
                            id="OnOdemeTutari"
                            name="OnOdemeTutari"
                            value={form.OnOdemeTutariVar ? form.OnOdemeTutari : ''}
                            onChange={TutarInputHandler}
                            type="text"
                            variant="outlined"
                            fullWidth
                            disabled={!form.OnOdemeTutariVar}
                            InputProps={{
                                startAdornment: <InputAdornment position="start">TL</InputAdornment>
                            }}
                        />
                    </div>
                    <div className="flex">
                        <div className="min-w-48 pt-20">
                            <Icon color="action">account_circle</Icon>
                        </div>
                        <Checkbox
                            className="mb-24"
                            checked={form.SabitTutarVar}
                            name="SabitTutarVar"
                            onChange={handleChange}
                            tabIndex={-1}
                            disableRipple
                            size="medium"
                        />
                        <TextField
                            className="mb-24"
                            label="Sabit Tutar"
                            id="SabitTutar"
                            name="SabitTutar"
                            value={form.SabitTutarVar ? form.SabitTutar : ''}
                            onChange={TutarInputHandler}
                            type="text"
                            variant="outlined"
                            fullWidth
                            disabled={!form.SabitTutarVar}
                            InputProps={{
                                startAdornment: <InputAdornment position="start">TL</InputAdornment>
                            }}
                        />
                    </div>
                    <div className="flex">
                        <div className="min-w-48 pt-20">
                            <Icon color="action">account_circle</Icon>
                        </div>
                        <Checkbox
                            className="mb-24"
                            checked={form.RaporBasiOdemeTutariVar}
                            name="RaporBasiOdemeTutariVar"
                            onChange={handleChange}
                            tabIndex={-1}
                            disableRipple
                            size="medium"
                        />
                        <TextField
                            className="mb-24"
                            label="Rapor Başı Ödeme Tutarı"
                            id="RaporBasiOdemeTutari"
                            name="RaporBasiOdemeTutari"
                            value={form.RaporBasiOdemeTutariVar ? form.RaporBasiOdemeTutari : ''}
                            onChange={TutarInputHandler}
                            type="text"
                            variant="outlined"
                            fullWidth
                            disabled={!form.RaporBasiOdemeTutariVar}
                            InputProps={{
                                startAdornment: <InputAdornment position="start">TL</InputAdornment>
                            }}
                        />
                    </div>
                    <div className="flex">
                        <div className="min-w-48 pt-20">
                            <Icon color="action">account_circle</Icon>
                        </div>
                        <Checkbox
                            className="mb-24"
                            checked={form.YuzdeTutariVar}
                            name="YuzdeTutariVar"
                            onChange={handleChange}
                            tabIndex={-1}
                            disableRipple
                            size="medium"
                        />
                        <TextField
                            className="mb-24"
                            label="Yüzde Tutarı"
                            id="YuzdeTutari"
                            name="YuzdeTutari"
                            value={form.YuzdeTutariVar? form.YuzdeTutari: ''}
                            onChange={TutarInputHandler}
                            type="text"
                            variant="outlined"
                            fullWidth
                            disabled={!form.YuzdeTutariVar}
                            InputProps={{
                                startAdornment: <InputAdornment position="start">TL</InputAdornment>
                            }}
                        />
                    </div>
                    <div className="flex">
                        <div className="min-w-48 pt-20">
                            <Icon color="action">account_circle</Icon>
                        </div>
                        <TextField
                            className="mb-24"
                            label="KDV Oranı"
                            id="KDVOrani"
                            name="KDVOrani"
                            value={form.KDVOrani}
                            onChange={TutarInputHandler}
                            type="text"
                            variant="outlined"
                            fullWidth
                            InputProps={{
                                startAdornment: <InputAdornment position="start">%</InputAdornment>
                            }}
                        />
                    </div>
                </DialogContent>

                <DialogActions className="justify-between p-8">
                    <div className="px-16">
                        <Button
                            variant="contained"
                            color="primary"
                            onClick={handleSubmit}
                        >
                            Ekle
                        </Button>
                        <Button
                            variant="contained"
                            color="primary"
                            onClick={() => { 
                                dispatch(Actions.closeYeniUrunDlg()) 
                            }}
                        >
                            Kapat
                        </Button>
                    </div>
                </DialogActions>
            </form>

        </Dialog >

    )
}

export default YeniUrunDlg