import React from 'react';

export const UrunAppConfig = {
    settings: {
        layout: {}
    },
    routes: [
        {
            path: '/apps/urun/:urunId',
            component: React.lazy(() => import('./urun/UrunDetail'))
        },
        {
            path: '/apps/urunler',
            component: React.lazy(() => import('./urunler/Urunler'))
        }
    ]
};
