import React from 'react';
import {Redirect} from 'react-router-dom';

export const SatisBoardAppConfig = {
    settings: {
        layout: {}
    },
    routes  : [
        {
            path     : '/apps/satis/boards/:boardId/:boardUri?',
            component: React.lazy(() => import('./board/Board'))
        },
        {
            path     : '/apps/satis/boards',
            component: React.lazy(() => import('./boards/Boards'))
        },
        {
            path     : '/apps/satisboard',
            component: () => <Redirect to="/apps/satis/boards"/>
        }
    ]
};
