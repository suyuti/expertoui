import React, {useEffect, useState} from 'react';
import {Button, MenuItem, IconButton, Icon, TextField, ClickAwayListener, InputAdornment} from '@material-ui/core';
import * as Actions from '../store/actions';
import {useForm} from '@fuse/hooks';
import {useDispatch, useSelector} from 'react-redux';
import {FuseLoading} from '@fuse';

import SatisReducer  from '../../satis/store/reducers';
import withReducer from 'app/store/withReducer';

function BoardAddCard(props)
{
    const dispatch      = useDispatch();
    const board         = useSelector(({scrumboardApp}) => scrumboardApp.board);
    const musteriler    = useSelector(({satisApp}) => satisApp.musteriler.data);

    const [formOpen, setFormOpen] = useState(false);
    const {form, handleChange, resetForm} = useForm({
        title: ''
    });


    useEffect(() => {
        if ( !formOpen )
        {
            resetForm();
        }
    }, [formOpen, resetForm]);

    function handleOpenForm()
    {
        setFormOpen(true);
    }

    function handleCloseForm()
    {
        setFormOpen(false);
    }

    function handleSubmit(ev)
    {
        ev.preventDefault();

        console.log(form)
        dispatch(Actions.newCard(board.id, props.listId, form.title, form.Firma))
            .then(() => {
                props.onCardAdded();
            });
        handleCloseForm();
    }

    function isFormInvalid()
    {
        return form.title.length === 0;
    }

    if (!musteriler) {
        return <FuseLoading />
    }

    return (
        <div className="w-full border-t-1">
            {formOpen ? (
                    <form className="p-16" 
                    onSubmit={handleSubmit}
                    >

                        <TextField
                            className="mb-16"
                            required
                            fullWidth
                            variant="outlined"
                            label="Satış Fırsatı Adı"
                            autoFocus
                            name="title"
                            value={form.title}
                            onChange={handleChange}
                            InputProps={{
                                endAdornment: (
                                    <InputAdornment position="end">
                                        <IconButton onClick={handleCloseForm}>
                                            <Icon className="text-18">close</Icon>
                                        </IconButton>
                                    </InputAdornment>
                                )
                            }}
                        />
                        <TextField
                            className="mb-16"
                            required
                            fullWidth
                            variant="outlined"
                            label="Musteri"
                            name="Firma"
                            value={form.Firma}
                            onChange={handleChange}
                            select
                        >
                            {musteriler.map((m, i) => (
                                <MenuItem key={m._id} value={m._id}>{m.Markasi}</MenuItem>
                            ))}

                        </TextField>

                        <div className="flex justify-between items-center">
                            <Button
                                variant="contained"
                                color="secondary"
                                type="submit"
                                disabled={isFormInvalid()}
                            >
                                Add
                            </Button>

                        </div>
                    </form>
            ) : (
                <Button
                    onClick={handleOpenForm}
                    classes={{
                        root : "normal-case font-600 w-full rounded-none h-48",
                        label: "justify-start"
                    }}
                >
                    <Icon className="text-20">add</Icon>
                    <span className="mx-8">Satış Fırsatı</span>
                </Button>
            )}
        </div>
    );
}

export default withReducer('satisApp', SatisReducer)(BoardAddCard);
