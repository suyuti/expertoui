import React, {useEffect} from 'react'
import { useDispatch, useSelector } from 'react-redux';
import KisiDlg from '../kisi/KisiDlg';
import withReducer from 'app/store/withReducer';
import reducer from '../store/reducers';
import * as Actions from '../store/actions';
import ReactTable from "react-table";
import {Button} from '@material-ui/core'
import { FuseLoading } from '@fuse';

function KisilerList(props) {
    const dispatch          = useDispatch();
    const kisiler           = useSelector(({ satisApp }) => satisApp.kisiler.data);

    useEffect(() => {
        function getKisiler() {
            dispatch(Actions.getKisiler())
        }
        getKisiler()
    }, [])

    if (!kisiler) {
        return <FuseLoading />
    }

    return (
        <React.Fragment>
            <ReactTable 
                className="-striped -highlight h-full sm:rounded-16 overflow-hidden"
                data={kisiler}
                columns={[
                    {
                        Header    : "Adi",
                        accessor  : "Adi",
                        filterable: true
                    },
                    {
                        Header    : "Soyadi",
                        accessor  : "Soyadi",
                        filterable: true
                    },
                    {
                        Header    : "Firma",
                        accessor  : "Musteri.Markasi",
                        filterable: true
                    },
                    {
                        Header    : "Yetki",
                        accessor  : "Yetki",
                        filterable: false
                    },
                    {
                        Header    : "Cep Telefonu",
                        accessor  : "CepTelefonu",
                        filterable: false
                    },
                ]}
                defaultPageSize={10}
            />
        </React.Fragment>
    )
}

export default withReducer('satisApp', reducer)(KisilerList);
