import React, {useEffect, useRef} from 'react';
import {Fab, Icon} from '@material-ui/core';
import {FusePageSimple, FuseAnimate} from '@fuse';
import withReducer from 'app/store/withReducer';
import * as Actions from '../store/actions';
import reducer from '../store/reducers';
import {makeStyles} from '@material-ui/styles';
import { useDispatch, useSelector } from 'react-redux';

import KisilerList from './KisilerList';
import KisiDlg from '../kisi/KisiDlg';
import KisilerHeader from './KisilerHeader';
//import ContactsSidebarContent from './ContactsSidebarContent';
//import ContactDialog from './ContactDialog';

const useStyles = makeStyles({
    addButton: {
        position: 'absolute',
        right   : 12,
        bottom  : 12,
        zIndex  : 99
    }
});

function Kisiler(props)
{
    const dispatch = useDispatch();
    const showKisiDlg       = useSelector(({ satisApp }) => satisApp.kisiler.showKisiDlg);

    const classes = useStyles(props);
    const pageLayout = useRef(null);

//    useEffect(() => {
//        dispatch(Actions.getContacts(props.match.params));
//        dispatch(Actions.getUserData());
//    }, [dispatch, props.match.params]);

    useEffect(() => {
//        dispatch(Actions.getContacts(props.match.params));
    }, [dispatch, props.match.params]);

    function onYeniKisiDlg() {
        dispatch(Actions.showKisiDlg())
    }

    return (
        <React.Fragment>
            <FusePageSimple
                classes={{
                    contentWrapper: "p-0 sm:p-24 pb-80 sm:pb-80 h-full",
                    content       : "flex flex-col h-full",
                    leftSidebar   : "w-256 border-0",
                    header        : "min-h-72 h-72 sm:h-136 sm:min-h-136"
                }}
                header={
                    <KisilerHeader pageLayout={pageLayout}  onYeniKisiDlg={onYeniKisiDlg}/>
                }
                content={
                    <KisilerList/>
                }
                sidebarInner
                ref={pageLayout}
                innerScroll
            />
            <KisiDlg show={showKisiDlg}/>
        </React.Fragment>
    )
}

export default withReducer('satisApp', reducer)(Kisiler);
