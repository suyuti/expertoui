import React from 'react';
import {Fab, Icon} from '@material-ui/core';
import {FusePageCarded} from '@fuse';
import {FusePageSimple, FuseAnimate} from '@fuse';
import withReducer from 'app/store/withReducer';
import MusterilerTable from './MusterilerTable';
import MusterilerHeader from './MusterilerHeader';
import reducer from '../store/reducers';
import YeniMusteriDlg from '../musteri/YeniMusteriDlg';
import { useDispatch, useSelector } from 'react-redux';
import * as Actions from '../store/actions';

function Musteriler()
{
    const dispatch = useDispatch();

    function openYeniMusteriDlg() {
        dispatch(Actions.openYeniMusteriDlg())
    }

    return (
        <React.Fragment>
        <FusePageCarded
            classes={{
                content: "flex",
                header : "min-h-72 h-72 sm:h-136 sm:min-h-136"
            }}
            header={
                <MusterilerHeader openYeniMusteriDlgHandler={openYeniMusteriDlg}/>
            }
            content={
                <MusterilerTable/>
            }
            innerScroll
        />
        <YeniMusteriDlg />
        </React.Fragment>
    );
}

export default withReducer('satisApp', reducer)(Musteriler);
