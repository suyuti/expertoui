import React, {useEffect, useState} from 'react';
import {Chip, Icon, Table, TableBody, TableCell, TablePagination, TableRow, Checkbox} from '@material-ui/core';
import {FuseScrollbars} from '@fuse';
import {withRouter} from 'react-router-dom';
import clsx from 'clsx';
import _ from '@lodash';
import MusterilerTableHead from './MusterilerTableHead';
import * as Actions from '../store/actions';
import {useDispatch, useSelector} from 'react-redux';
import WarningIcon from '@material-ui/icons/Warning';

function MusterilerTable(props)
{
    const dispatch      = useDispatch();
    const musteriler    = useSelector(({satisApp}) => satisApp.musteriler.data);
    const searchText    = useSelector(({satisApp}) => satisApp.musteriler.searchText);

    const [selected, setSelected] = useState([]);
    const [data, setData] = useState(musteriler);
    const [page, setPage] = useState(0);
    const [rowsPerPage, setRowsPerPage] = useState(10);
    const [order, setOrder] = useState({
        direction: 'asc',
        id       : null
    });

    useEffect(() => {
        dispatch(Actions.getMusteriler(props.match.params));
    }, [dispatch, props.match.params]);

    useEffect(() => {
        if ( searchText.length !== 0 )
        {
            setData(_.filter(musteriler, item => item.name.toLowerCase().includes(searchText.toLowerCase())));
            setPage(0);
        }
        else
        {
            setData(musteriler);
        }
    }, [musteriler, searchText]);

    function handleRequestSort(event, property)
    {
        const id = property;
        let direction = 'desc';

        if ( order.id === property && order.direction === 'desc' )
        {
            direction = 'asc';
        }

        setOrder({
            direction,
            id
        });
    }

    function handleSelectAllClick(event)
    {
        if ( event.target.checked )
        {
            setSelected(data.map(n => n.id));
            return;
        }
        setSelected([]);
    }

    function handleClick(item)
    {
        props.history.push('/apps/satis/musteri/' + item._id 
        //+ '/' + item.handle
        );
    }

    function handleCheck(event, id)
    {
        const selectedIndex = selected.indexOf(id);
        let newSelected = [];

        if ( selectedIndex === -1 )
        {
            newSelected = newSelected.concat(selected, id);
        }
        else if ( selectedIndex === 0 )
        {
            newSelected = newSelected.concat(selected.slice(1));
        }
        else if ( selectedIndex === selected.length - 1 )
        {
            newSelected = newSelected.concat(selected.slice(0, -1));
        }
        else if ( selectedIndex > 0 )
        {
            newSelected = newSelected.concat(
                selected.slice(0, selectedIndex),
                selected.slice(selectedIndex + 1)
            );
        }

        setSelected(newSelected);
    }

    function handleChangePage(event, page)
    {
        setPage(page);
    }

    function handleChangeRowsPerPage(event)
    {
        setRowsPerPage(event.target.value);
    }


    return (
        <div className="w-full flex flex-col">

            <FuseScrollbars className="flex-grow overflow-x-auto">

                <Table className="min-w-xl" aria-labelledby="tableTitle">

                    <MusterilerTableHead
                        numSelected={selected.length}
                        order={order}
                        onSelectAllClick={handleSelectAllClick}
                        onRequestSort={handleRequestSort}
                        rowCount={data.length}
                    />

                    <TableBody>
                        {
                        _.orderBy(data, [
                            (o) => {
                                switch ( order.id )
                                {
                                    case 'categories':
                                    {
                                        return o.categories[0];
                                    }
                                    default:
                                    {
                                        return o[order.id];
                                    }
                                }
                            }
                        ], [order.direction])
                            .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                            .map(n => {
                                var statusChip = <></>
                                if (n.MusteriStatus === 'potansiyel') {
                                    statusChip = <Chip color="secondary" variant="outlined" label="Potansiyel" size="small" />
                                }
                                else if (n.MusteriStatus === 'aktif') {
                                    statusChip = <Chip color="primary" label="Aktif" size="small"  variant="outlined"/>
                                }
                                else if (n.MusteriStatus === 'pasif') {
                                    statusChip = <Chip  label="Pasif" size="small"  variant="outlined"/>
                                }
                                else if (n.MusteriStatus === 'sorunlu') {
                                    statusChip = <Chip color="primary" label="Sorunlu" size="small"  variant="outlined"/>
                                }



                                const isSelected = selected.indexOf(n._id) !== -1;
                                return (
                                    <TableRow
                                        className="h-64 cursor-pointer"
                                        hover
                                        role="checkbox"
                                        aria-checked={isSelected}
                                        tabIndex={-1}
                                        key={n._id}
                                        selected={isSelected}
                                        onClick={event => handleClick(n)}
                                    >
                                        <TableCell className="w-64 text-center" padding="none">
                                            <Checkbox
                                                checked={isSelected}
                                                onClick={event => event.stopPropagation()}
                                                onChange={event => handleCheck(event, n._id)}
                                            />
                                        </TableCell>
                                        
                                        <TableCell className="w-52" component="th" scope="row" padding="none">
                                            {/*n.images.length > 0 && n.featuredImageId ? (
                                                <img className="w-full block rounded" src={_.find(n.images, {id: n.featuredImageId}).url} alt={n.name}/>
                                            ) : (
                                                <img className="w-full block rounded" src="assets/images/ecommerce/product-image-placeholder.png" alt={n.name}/>
                                            )*/}
                                        </TableCell>
                                        
                                        <TableCell component="th" scope="row">
                                            {n.Markasi}
                                        </TableCell>

                                        <TableCell className="truncate" component="th" scope="row">
                                            {n.Unvani}
                                        </TableCell>

                                        <TableCell className="truncate" component="th" scope="row">
                                            {n.Sektoru && n.Sektoru.Adi}
                                        </TableCell>

                                        <TableCell className="truncate" component="th" scope="row">
                                            {statusChip}
                                        </TableCell>

                                    </TableRow>
                                );
                            })}
                    </TableBody>
                </Table>
            </FuseScrollbars>

            <TablePagination
                className="overflow-hidden"
                component="div"
                count={data.length}
                rowsPerPage={rowsPerPage}
                page={page}
                backIconButtonProps={{
                    'aria-label': 'Previous Page'
                }}
                nextIconButtonProps={{
                    'aria-label': 'Next Page'
                }}
                onChangePage={handleChangePage}
                onChangeRowsPerPage={handleChangeRowsPerPage}
            />
        </div>
    );
}

export default withRouter(MusterilerTable);
