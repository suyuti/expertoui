import React from 'react';
import {Paper, Button, Input, Icon, Typography} from '@material-ui/core';
import {ThemeProvider} from '@material-ui/styles';
import {FuseAnimate} from '@fuse';
import {useDispatch, useSelector} from 'react-redux';
import {Link} from 'react-router-dom';
import * as Actions from '../store/actions';
import {withRouter} from 'react-router-dom';
import * as Permission from 'app/main/helpers/Permission'

function MusterilerHeader(props)
{
    const dispatch = useDispatch();
    const searchText = useSelector(({satisApp}) => satisApp.musteriler.searchText);
    const mainTheme = useSelector(({fuse}) => fuse.settings.mainTheme);
    const user = useSelector(({auth}) => auth.user);

    return (
        <div className="flex flex-1 w-full items-center justify-between">

            <div className="flex items-center">
                <FuseAnimate animation="transition.expandIn" delay={300}>
                    <Icon className="text-32">shopping_basket</Icon>
                </FuseAnimate>
                <FuseAnimate animation="transition.slideLeftIn" delay={300}>
                    <Typography className="hidden sm:flex mx-0 sm:mx-12 capitalize" variant="h6">{props.match.params.MusteriStatus} Müşteriler</Typography>
                </FuseAnimate>
            </div>

            <div className="flex flex-1 items-center justify-center px-12">

                <ThemeProvider theme={mainTheme}>
                    <FuseAnimate animation="transition.slideDownIn" delay={300}>
                        <Paper className="flex items-center w-full max-w-512 px-8 py-4 rounded-8" elevation={1}>

                            <Icon color="action">search</Icon>

                            <Input
                                placeholder="Ara"
                                className="flex flex-1 mx-8"
                                disableUnderline
                                fullWidth
                                value={searchText}
                                inputProps={{
                                    'aria-label': 'Search'
                                }}
                                onChange={ev => dispatch(Actions.setMusterilerSearchText(ev))}
                            />
                        </Paper>
                    </FuseAnimate>
                </ThemeProvider>

            </div>
            <FuseAnimate animation="transition.slideRightIn" delay={300}>
                <Button 
                    //component={Link} to="/apps/satis/musteri/new" 
                    className="whitespace-no-wrap normal-case" 
                    variant="contained" 
                    color="secondary"
                    onClick={() => {props.openYeniMusteriDlgHandler()}}
                    disabled={!Permission.can(user.role, Permission.MUSTERI_EKLER)}
                    >
                    <span className="hidden sm:flex">Yeni Musteri Ekle</span>
                    <span className="flex sm:hidden">Yeni</span>
                </Button>
            </FuseAnimate>
        </div>
    );
}

export default withRouter(MusterilerHeader);
