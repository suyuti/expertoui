import React from 'react';
import { Redirect } from 'react-router-dom';

export const SatisAppConfig = {
    settings: {
        layout: {}
    },
    routes: [
        //  {
        //      path     : '/apps/satis/musteriler/:mId/:mHandle?',
        //      component: React.lazy(() => import('./musteri/Musteri'))
        //  },

        {
            path: '/apps/satis/musteriler/:MusteriStatus',
            component: React.lazy(() => import('./musteriler/Musteriler'))
        },
        {
            path     : '/apps/satis/musteri/:musteriId',
            component: React.lazy(() => import('./musteri/MusteriDetail'))
        },
        {
            path     : '/apps/satis/kisi/:mid/:kid',
            component: React.lazy(() => import('./kisi/KisiDetail'))
        },
        {
            path     : '/apps/satis/kisiler',
            component: React.lazy(() => import('./kisiler/Kisiler'))
        },
        {
            path: '/apps/satis/yonetim',
            component: React.lazy(() => import('./dashboards/SatisYonetimDashboard'))
        },
        {
            path: '/apps/satis',
            component: React.lazy(() => import('./dashboards/SatisDashboard'))
        },


        /*
        
            
            {
                path     : '/apps/satis/musteriler/:MusteriStatus',
                component: React.lazy(() => import('./musteriler/Musteriler'))
            },
            {
                path     : '/apps/satis/kisi/:mid/:kid',
                component: React.lazy(() => import('./kisi/KisiDetail'))
            },
            {
                path     : '/apps/satis/kisiler',
                component: React.lazy(() => import('./kisiler/Kisiler'))
            },
            */
        /*    {
                    path     : '/apps/satis',
                    component: () => <Redirect to="/apps/satis/musteriler"/>
                }
                */
    ]
};
