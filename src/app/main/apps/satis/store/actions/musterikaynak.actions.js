import {http} from 'app/services/axiosService/axios'
import {FuseUtils} from '@fuse';
import {showMessage} from 'app/store/actions/fuse';

export const GET_MUSTERIKAYNAK_LIST = '[SATIS APP] GET MUSTERIKAYNAK LIST';

export function getMusteriKaynakList(params)
{
    const request = http.get('/api/musterikaynak', {params});

    return (dispatch) =>
        request.then((response) =>
        {
            dispatch({
                type   : GET_MUSTERIKAYNAK_LIST,
                payload: response
            })
        }
        );
}
