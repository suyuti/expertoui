import {http} from 'app/services/axiosService/axios'
import {FuseUtils} from '@fuse';
import {showMessage} from 'app/store/actions/fuse';
import { SHOW_YENI_GORUSME_DLG } from './gorusmeler.actions';

export const GET_KISILER            = '[SATIS APP] GET KISILER';
export const GET_MUSTERI_KISILER    = '[SATIS APP] GET MUSTERI KISILER';
export const MUSTERI_KISI_EKLE      = '[SATIS APP] MUSTERI KISI EKLE';
export const SHOW_YENI_KISI_DLG     = '[SATIS APP] SHOW YENI KISI DLG';
export const HIDE_YENI_KISI_DLG     = '[SATIS APP] HIDE YENI KISI DLG';
export const CLEAR_KISILER          = '[SATIS APP] CLEAR KISILER';

export function getKisiler()
{
    const request = http.get(`/api/kisi`);

    return (dispatch) =>
        request.then((response) =>
            {
            dispatch({
                type   : GET_KISILER,
                payload: response
            })}
        );
}

export function getMusteriKisiler(musteriId)
{
    const request = http.get(`/api/kisi/${musteriId}`);

    return (dispatch) =>
        request.then((response) =>
            {
            dispatch({
                type   : GET_MUSTERI_KISILER,
                payload: response
            })}
        );
}

export function musteriKisiEkle(musteriId, kisi)
{
    const request = http.post(`/api/kisi/${musteriId}`, kisi);

    return (dispatch) =>
        request.then((response) =>
            {
            dispatch({
                type   : MUSTERI_KISI_EKLE,
                payload: response
            })}
        );
}

export function showKisiDlg(){
    return {
        type: SHOW_YENI_KISI_DLG
    }
}

export function hideKisiDlg(){
    return {
        type: HIDE_YENI_KISI_DLG
    }
}

export function clearKisiler(){
    return {
        type: CLEAR_KISILER
    }
}
