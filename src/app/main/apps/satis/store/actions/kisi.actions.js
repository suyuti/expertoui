import {http} from 'app/services/axiosService/axios'
import {FuseUtils} from '@fuse';
import {showMessage} from 'app/store/actions/fuse';
import { SHOW_YENI_GORUSME_DLG } from './gorusmeler.actions';

export const GET_KISI            = '[SATIS APP] GET KISI';
export const CLEAR_KISI          = '[SATIS APP] CLEAR KISI';

export function getKisi(musteriId, id)
{
    const request = http.get(`/api/kisi/${musteriId}/${id}`);

    return (dispatch) =>
        request.then((response) =>
            {
            dispatch({
                type   : GET_KISI,
                payload: response
            })}
        );
}

export function clearKisi(){
    return {
        type: CLEAR_KISI
    }
}
