import {http} from 'app/services/axiosService/axios'

export const GET_MUSTERILER             = '[SATIS APP] GET MUSTERILER';
export const SET_MUSTERILER_SEARCH_TEXT = '[SATIS APP] SET MUSTERILER SEARCH TEXT';

export function getMusteriler(routeParams)
{
    const request = http.get('/api/satis/musteriler', {params: routeParams});

    return (dispatch) =>
        request.then((response) =>
            {
                //console.log(response)
                //if (response.status === 200) {
                    dispatch({
                        type   : GET_MUSTERILER,
                        payload: response
                    })
                //}
            }
        );
}

export function setMusterilerSearchText(event)
{
    return {
        type      : SET_MUSTERILER_SEARCH_TEXT,
        searchText: event.target.value
    }
}

