import {http} from 'app/services/axiosService/axios'
import {FuseUtils} from '@fuse';
import {showMessage} from 'app/store/actions/fuse';

export const GET_SEKTOR_LIST = '[SATIS APP] GET SEKTOR LIST';

export function getSektorler(params)
{
    const request = http.get('/api/sektor', {params});

    return (dispatch) =>
        request.then((response) =>
        {
            dispatch({
                type   : GET_SEKTOR_LIST,
                payload: response
            })
        }
        );
}
