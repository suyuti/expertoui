import {http} from 'app/services/axiosService/axios'
import {FuseUtils} from '@fuse';
import {showMessage} from 'app/store/actions/fuse';

export const GET_GORUSME_LIST   = '[SATIS APP] GET GORUSME LIST';
export const GORUSME_EKLE       = '[SATIS APP] GORUSME EKLE';
export const GORUSME_SIL        = '[SATIS APP] GORUSME SIL';

export const SHOW_YENI_GORUSME_DLG        = '[SATIS APP] SHOW YENI GORUSME DLG';
export const HIDE_YENI_GORUSME_DLG        = '[SATIS APP] HIDE YENI GORUSME DLG';

export function getMusteriGorusmeList(musteriId)
{
    const request = http.get(`/api/gorusme/${musteriId}`);

    return (dispatch) =>
        request.then((response) =>
            {
            dispatch({
                type   : GET_GORUSME_LIST,
                payload: response//.data
            })}
        );
}

export function musteriGorusmeEkle(musteriId, gorusme)
{
    const request = http.post(`/api/gorusme/${musteriId}`, gorusme);

    return (dispatch) =>
        request.then((response) =>
            {
            dispatch({
                type   : GORUSME_EKLE,
                payload: response//.data
            })}
        );
}

export function musteriGorusmeSil(musteriId, gorusme)
{
    const request = http.delete(`/api/gorusme/${musteriId}/${gorusme._id}`);

    return (dispatch) =>
        request.then((response) =>
            {
            dispatch({
                type   : GORUSME_SIL,
                payload: response//.data
            })}
        );
}

export function showYeniGorusmeDlg() {
    return {
        type: SHOW_YENI_GORUSME_DLG
    }
}

export function hideYeniGorusmeDlg() {
    return {
        type: HIDE_YENI_GORUSME_DLG
    }
}