import {http} from 'app/services/axiosService/axios'
import {FuseUtils} from '@fuse';
import {showMessage} from 'app/store/actions/fuse';

export const GET_MUSTERI = '[SATIS APP] GET MUSTERI';
export const UPDATE_MUSTERI = '[SATIS APP] UPDATE MUSTERI';
export const SAVE_MUSTERI = '[SATIS APP] SAVE MUSTERI';
export const OPEN_NEW_MUSTERI_DIALOG = '[SATIS APP] OPEN NEW MUSTERI DIALOG';
export const CLOSE_NEW_MUSTERI_DIALOG = '[SATIS APP] CLOSE NEW MUSTERI DIALOG';
export const ADRES_EKLE = '[SATIS APP] ADRES EKLE';
export const ADRES_SIL = '[SATIS APP] ADRES SIL';

export const OPEN_NEW_ADRES_DIALOG = '[SATIS APP] OPEN NEW ADRES DIALOG';
export const CLOSE_NEW_ADRES_DIALOG = '[SATIS APP] CLOSE NEW ADRES DIALOG';

export const GET_MUSTERI_ADRESLERI = '[SATIS APP] GET MUSTERI ADRESLERI';

export const GET_MUSTERI_SOZLESMELER = '[SATIS APP] GET MUSTERI SOZLESMELER';

export function getMusteri(params)
{
    const request = http.get('/api/satis/musteri', {params});

    return (dispatch) =>
        request.then((response) =>
            {
            dispatch({
                type   : GET_MUSTERI,
                payload: response[0]//.data
            })}
        );
}

export function saveMusteri(data)
{
    const request = http.post('/api/satis/musteri/save', data);

    return (dispatch) =>
        request.then((response) => {

                dispatch(showMessage({message: 'Musteri Kaydedildi'}));

                return dispatch({
                    type   : SAVE_MUSTERI,
                    payload: response.data
                })
            }
        );
}

export function updateMusteri(data)
{
    console.log(data)
    const request = http.put(`/api/satis/musteri/${data._id}`, data);

    return (dispatch) =>
        request.then((response) => {

                dispatch(showMessage({message: 'Musteri Güncellendi'}));

                return dispatch({
                    type   : UPDATE_MUSTERI,
                    payload: response
                })
            }
        );
}

export function openYeniMusteriDlg()
{
    return {
        type: OPEN_NEW_MUSTERI_DIALOG
    }
}

export function closeYeniMusteriDlg()
{
    return {
        type: CLOSE_NEW_MUSTERI_DIALOG
    }
}

export function adresEkle(data)
{
    const request = http.post(`/api/satis/musteri/${data.musteriId}/adres`, data);

    return (dispatch) =>
        request.then((response) => {

                dispatch(showMessage({message: 'Adres eklendi'}));
                dispatch(getMusteriAdresleri(data.musteriId))

                return dispatch({
                    type   : ADRES_EKLE,
                    payload: response
                })
            }
        );
}

export function openAdresDlg()
{
    return {
        type: OPEN_NEW_ADRES_DIALOG
    }
}

export function closeAdresDlg()
{
    return {
        type: CLOSE_NEW_ADRES_DIALOG
    }
}

export function getMusteriAdresleri(musteriID)
{
    const request = http.get(`/api/satis/musteri/${musteriID}/adres`);

    return (dispatch) =>
        request.then((response) => {
                return dispatch({
                    type   : GET_MUSTERI_ADRESLERI,
                    payload: response
                })
            }
        );
}

export function adresSil(musteriId, adresId)
{
    const request = http.delete(`/api/satis/musteri/${musteriId}/adres/${adresId}`);

    return (dispatch) =>
        request.then((response) => {

                dispatch(showMessage({message: 'Adres silindi'}));
                dispatch(getMusteriAdresleri(musteriId))

                return dispatch({
                    type   : ADRES_SIL,
                })
            }
        );
}

export function getMusteriSozlesmeler(musteriId)
{
    const request = http.get(`/api/satis/musteri/${musteriId}/sozlesme`);

    return (dispatch) =>
        request.then((response) => {
                return dispatch({
                    type   : GET_MUSTERI_SOZLESMELER,
                    sozlesmeler: response
                })
            }
        );
}

export function newMusteri()
{
    const data = {
        id              : FuseUtils.generateGUID(),
        name            : '',
        handle          : '',
        description     : '',
        categories      : [],
        tags            : [],
        images          : [],
        priceTaxExcl    : 0,
        priceTaxIncl    : 0,
        taxRate         : 0,
        comparedPrice   : 0,
        quantity        : 0,
        sku             : '',
        width           : '',
        height          : '',
        depth           : '',
        weight          : '',
        extraShippingFee: 0,
        active          : true
    };

    return {
        type   : GET_MUSTERI,
        payload: data
    }
}
