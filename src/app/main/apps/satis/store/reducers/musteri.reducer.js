import * as Actions from '../actions';

const initialState = {
    data: null,
    showYeniMusteriDlg: false,
    showAdresDlg: false,
    sozlesmeler: null
};

const musteriReducer = function (state = initialState, action) {
    switch (action.type) {
        case Actions.GET_MUSTERI:
            {
                return {
                    ...state,
                    data: action.payload
                };
            }
        case Actions.SAVE_MUSTERI:
            {
                return {
                    ...state,
                    data: action.payload
                };
            }
        case Actions.UPDATE_MUSTERI:
            {
                return {
                    ...state,
                    data: action.payload
                };
            }
        case Actions.OPEN_NEW_MUSTERI_DIALOG:
            return {
                ...state,
                showYeniMusteriDlg: true
            }
        case Actions.CLOSE_NEW_MUSTERI_DIALOG:
            return {
                ...state,
                showYeniMusteriDlg: false
            }
        case Actions.OPEN_NEW_ADRES_DIALOG:
            {
                return {
                    ...state,
                    showAdresDlg: true
                }
            }
        case Actions.CLOSE_NEW_ADRES_DIALOG:
            {
                return {
                    ...state,
                    showAdresDlg: false
                }
            }

        case Actions.GET_MUSTERI_ADRESLERI: {
            var musteri = {...state.data}
            musteri.Adresler = [...action.payload]
            return {
                ...state,
                data: {...musteri}
            }
        }
        case Actions.ADRES_EKLE : {
            return {
                ...state,
                showAdresDlg: false
            }
        }
        case Actions.GET_MUSTERI_SOZLESMELER: {
            return {
                ...state,
                sozlesmeler: action.sozlesmeler
            }
        }
        default:
            {
                return state;
            }
    }
};

export default musteriReducer;
