import * as Actions from '../actions';

const initialState = {
    data: null,
    showKisiDlg: false,
    searchText: ''
};

const kisilerReducer = function (state = initialState, action) {
    switch (action.type) {
        case Actions.GET_MUSTERI_KISILER:
            {
                return {
                    ...state,
                    data: action.payload
                };
            }
            case Actions.CLEAR_KISILER:
                {
                    return {
                        ...state,
                        data: null
                    }
                }
            case Actions.GET_KISILER:
                {
                    return {
                        ...state,
                        data: action.payload
                    };
                }
            case Actions.SHOW_YENI_KISI_DLG:
            {
                return {
                    ...state,
                    showKisiDlg: true
                }
            }
        case Actions.HIDE_YENI_KISI_DLG:
            {
                return {
                    ...state,
                    showKisiDlg: false
                }
            }
        default:
            {
                return state;
            }
    }
};

export default kisilerReducer;
