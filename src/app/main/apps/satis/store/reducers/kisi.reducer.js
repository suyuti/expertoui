import * as Actions from '../actions';

const initialState = {
    data: null,
};

const kisiReducer = function (state = initialState, action) {
    switch (action.type) {
            case Actions.CLEAR_KISI:
                {
                    return {
                        ...state,
                        data: null
                    }
                }
            case Actions.GET_KISI:
                {
                    return {
                        ...state,
                        data: action.payload
                    };
                }
        default:
            {
                return state;
            }
    }
};

export default kisiReducer;
