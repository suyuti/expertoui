import * as Actions from '../actions';

const initialState = {
    data      : [],
    searchText: ''
};

const musterilerReducer = function (state = initialState, action) {
    switch ( action.type )
    {
        case Actions.GET_MUSTERILER:
        {
            return {
                ...state,
                data: action.payload
            };
        }
        case Actions.SET_MUSTERILER_SEARCH_TEXT:
        {
            return {
                ...state,
                searchText: action.searchText
            };
        }
        default:
        {
            return state;
        }
    }
};

export default musterilerReducer;
