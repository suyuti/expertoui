import {combineReducers} from 'redux';
import musteriler from './musteriler.reducer';
import musteri from './musteri.reducer';
import sektorler from './sektorler.reducer';
import musteriKaynak from './musterikaynak.reducer';
import gorusmeler from './gorusmeler.reducer';
import kisiler from './kisiler.reducer';
import kisi from './kisi.reducer';

const reducer = combineReducers({
    sektorler,
    musteriler,
    musteri,
    musteriKaynak,
    gorusmeler,
    kisiler,
    kisi
});

export default reducer;
