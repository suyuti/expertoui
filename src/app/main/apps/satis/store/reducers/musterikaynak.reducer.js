import * as Actions from '../actions';

const initialState = {
    data: null
};

const musterikaynakReducer = function (state = initialState, action) {
    switch ( action.type )
    {
        case Actions.GET_MUSTERIKAYNAK_LIST:
        {
            return {
                ...state,
                data: action.payload
            };
        }
        default:
        {
            return state;
        }
    }
};

export default musterikaynakReducer;
