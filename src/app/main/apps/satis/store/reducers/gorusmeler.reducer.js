import * as Actions from '../actions';

const initialState = {
    data: null,
    showGorusmeDlg: false,
};

const gorusmeReducer = function (state = initialState, action) {
    switch (action.type) {
        case Actions.GET_GORUSME_LIST:
            {
                return {
                    ...state,
                    data: action.payload
                };
            }
        case Actions.GORUSME_EKLE:
            {
                return {
                    ...state,
                };
            }
        case Actions.GORUSME_SIL:
            {
                return {
                    ...state,
                };
            }
        case Actions.SHOW_YENI_GORUSME_DLG:
            {
                return {
                    ...state,
                    showGorusmeDlg: true
                };
            }
        case Actions.HIDE_YENI_GORUSME_DLG:
            {
                return {
                    ...state,
                    showGorusmeDlg: false
                };
            }
        default:
            {
                return state;
            }
    }
};

export default gorusmeReducer;
