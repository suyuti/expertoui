import React, { useState, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import * as Actions from '../store/actions';
import withReducer from 'app/store/withReducer';
import reducer from '../store/reducers';
import MaskedInput from 'react-text-mask'
import emailMask from 'text-mask-addons/dist/emailMask'
import {
    Dialog, AppBar, Toolbar, DialogContent, DialogActions,
    Paper,
    CardContent, Button, Tab,
    Tooltip, Input,
    Tabs, TextField, InputAdornment, Icon, Avatar, Typography, Card, Select, MenuItem, InputLabel, FormControl, Divider, CardActions, IconButton
} from '@material-ui/core';
import { FuseLoading } from '@fuse';

function KisiDlg(props) {
    const dispatch = useDispatch();
    const firmalar = useSelector(({ satisApp }) => satisApp.musteriler.data);

    const [kisi, setKisi] = useState({})

    const yetkiler = ['Yönetim Yetkilisi', 'Teknik Yetkilisi', 'Mali Yetkilisi']
    const cinsiyetler = ['Erkek', 'Kadın']

    useEffect(() => {
        function getInitials() {
            dispatch(Actions.getMusteriler())
        }
        getInitials()
    }, [])

    if (!firmalar) {
        return <FuseLoading />
    }

    function handleChange(e) {
        e.preventDefault()
        var a = { ...kisi }
        a[e.target.name] = e.target.value
        setKisi(a)
    }

    function handleSubmit() {
        dispatch(Actions.musteriKisiEkle(kisi))
    }

    return (
        <React.Fragment>
            <Dialog
                open={props.show}
                classes={{
                    paper: "m-24"
                }}
                fullWidth
            >
                <AppBar position="static" elevation={1}>
                    <Toolbar className="flex w-full">
                        <Typography variant="subtitle1" color="inherit">
                            Yeni Kisi Ekleme
                        </Typography>
                    </Toolbar>
                    <div className="flex flex-col items-center justify-center pb-24">
                    </div>
                </AppBar>
                <form noValidate
                    //onSubmit={handleSubmit} 
                    className="flex flex-col md:overflow-hidden">
                    <DialogContent classes={{ root: "p-24" }}>
                    <div className="flex">
                            <div className="min-w-48 pt-20">
                                <Icon color="action">account_circle</Icon>
                            </div>

                            <TextField
                                autoFocus
                                select
                                className="mb-24"
                                label="Firma"
                                id="Firma"
                                name="Firma"
                                value={kisi.Firma}
                                onChange={handleChange}
                                variant="outlined"
                                required
                                fullWidth
                            >
                                {
                                    firmalar.map((f,i) => (
                                        <MenuItem key={f} value={f}>{f.Markasi}</MenuItem>
                                    ))
                                }
                            </TextField>
                        </div>
                        <div className="flex">
                            <div className="min-w-48 pt-20">
                                <Icon color="action">account_circle</Icon>
                            </div>

                            <TextField
                                className="mb-24"
                                label="Adi"
                                id="Adi"
                                name="Adi"
                                value={kisi.Adi}
                                onChange={handleChange}
                                variant="outlined"
                                required
                                fullWidth
                            />
                            <TextField
                                className="mb-24 ml-12"
                                label="Soyadi"
                                id="Soyadi"
                                name="Soyadi"
                                value={kisi.Soyadi}
                                onChange={handleChange}
                                variant="outlined"
                                required
                                fullWidth
                            />
                        </div>
                        <div className="flex">
                            <div className="min-w-48 pt-20">
                                <Icon color="action">account_circle</Icon>
                            </div>
                            <TextField
                                className="mb-24"
                                label="Unvani"
                                id="Unvani"
                                name="Unvani"
                                value={kisi.Unvani}
                                onChange={handleChange}
                                variant="outlined"
                                required
                                fullWidth
                            />
                            <TextField
                                select
                                className="mb-24 ml-12"
                                label="Cinsiyet"
                                id="Cinsiyet"
                                name="Cinsiyet"
                                value={kisi.Cinsiyet}
                                onChange={handleChange}
                                variant="outlined"
                                required
                                fullWidth
                            >
                                {
                                    cinsiyetler.map((c,i) => (
                                        <MenuItem key={c} value={c}>{c}</MenuItem>
                                    ))
                                }
                            </TextField>
                        </div>
                        <div className="flex">
                            <div className="min-w-48 pt-20">
                                <Icon color="action">account_circle</Icon>
                            </div>

                            <TextField
                                select
                                className="mb-24"
                                label="Yetki"
                                id="Yetki"
                                name="Yetki"
                                value={kisi.Yetki}
                                onChange={handleChange}
                                variant="outlined"
                                required
                                fullWidth
                            >
                                {
                                    yetkiler.map((y,i) => (
                                        <MenuItem key={y} value={y}>{y}</MenuItem>
                                    ))
                                }
                            </TextField>
                        </div>
                        <div className="flex">
                            <div className="min-w-48 pt-20">
                                <Icon color="action">account_circle</Icon>
                            </div>
                            <TextField
                                className="mb-24"
                                label="Mail"
                                id="Mail"
                                name="Mail"
                                value={kisi.Mail}
                                onChange={handleChange}
                                variant="outlined"
                                required
                                fullWidth
                                InputProps={{
                                    inputComponent: (props) => (
                                        <MaskedInput
                                            {...props}
                                            mask={emailMask}
                                            placeholderChar={'\u2000'}
                                        //showMask
                                        ></MaskedInput>
                                    )
                                }}
                            />
                        </div>
                        <div className="flex">
                            <div className="min-w-48 pt-20">
                                <Icon color="action">account_circle</Icon>
                            </div>
                            <TextField
                                className="mb-24"
                                label="Cep telefonu"
                                id="CepTelefonu"
                                name="CepTelefonu"
                                variant="outlined"
                                value={kisi.CepTelefonu}
                                onChange={handleChange}
                                required
                                fullWidth
                                InputProps={{
                                    inputComponent: (props) => (
                                        <MaskedInput
                                            {...props}
                                            mask={['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]}
                                            placeholderChar={'\u2000'}
                                        //showMask
                                        ></MaskedInput>
                                    )
                                }}

                            ></TextField>
                        </div>
                        <div className="flex">
                            <div className="min-w-48 pt-20">
                                <Icon color="action">account_circle</Icon>
                            </div>
                            <TextField
                                className="mb-24"
                                label="Is Telefonu"
                                id="IsTelefonu"
                                name="IsTelefonu"
                                variant="outlined"
                                required
                                fullWidth
                                value={kisi.IsTelefonu}
                                onChange={handleChange}
                                InputProps={{
                                    inputComponent: (props) => (
                                        <MaskedInput
                                            {...props}
                                            mask={['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]}
                                            placeholderChar={'\u2000'}
                                        //showMask
                                        ></MaskedInput>
                                    )
                                }}

                            ></TextField>
                            <TextField
                                className="mb-24 ml-2"
                                label="Dahili"
                                id="Dahili"
                                name="Dahili"
                                value={kisi.Dahili}
                                onChange={handleChange}
                                variant="outlined"
                                required
                                fullWidth
                            />
                        </div>
                        <div className="flex">
                            <div className="min-w-48 pt-20">
                                <Icon color="action">account_circle</Icon>
                            </div>
                            <TextField
                                className="mb-24"
                                label="TCKN"
                                id="TCKN"
                                name="TCKN"
                                variant="outlined"
                                required
                                fullWidth
                                value={kisi.TCKN}
                                onChange={handleChange}

                                InputProps={{
                                    inputComponent: (props) => (
                                        <MaskedInput
                                            {...props}
                                            mask={[/\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/]}
                                            placeholderChar={'\u2000'}
                                        //showMask
                                        ></MaskedInput>
                                    )
                                }}

                            ></TextField>
                        </div>
                        <div className="flex">
                            <div className="min-w-48 pt-20">
                                <Icon color="action">account_circle</Icon>
                            </div>
                            <TextField
                                className="mb-24"
                                label="Dogum Tarihi"
                                id="DogumTarihi"
                                name="DogumTarihi"
                                variant="outlined"
                                required
                                fullWidth
                                value={kisi.DogumTarihi}
                                onChange={handleChange}
                                InputProps={{
                                    inputComponent: (props) => (
                                        <MaskedInput
                                            {...props}
                                            mask={[/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/]}
                                            placeholderChar={'\u2000'}
                                        //showMask
                                        ></MaskedInput>
                                    )
                                }}

                            ></TextField>
                        </div>

                    </DialogContent>

                    <DialogActions className="justify-between p-8">
                        <div className="px-16">
                            <Button
                                variant="contained"
                                color="primary"
                                onClick={() => {
                                    console.log(kisi)
                                    dispatch(Actions.musteriKisiEkle(kisi.Firma._id,kisi)) 
                                    //setAdres({ musteriId: props.musteriId })
                                }}
                            //type="submit"
                            //disabled={!canBeSubmitted()}
                            >Ekle</Button>
                            <Button
                                variant="contained"
                                color="primary"
                                onClick={() => {
                                    dispatch(Actions.hideKisiDlg())
                                    //setAdres({ musteriId: props.musteriId })
                                }}
                            //type="submit"
                            //disabled={!canBeSubmitted()}
                            >Kapat</Button>
                        </div>
                    </DialogActions>
                </form>

            </Dialog>
        </React.Fragment>
    )
}

export default KisiDlg
