import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import * as Actions from '../store/actions';
import { FuseAnimate, FuseLoading, FusePageSimple, FusePageCarded } from '@fuse';
import withReducer from 'app/store/withReducer';
import reducer from '../store/reducers';
import {
    List, ListItem, ListItemAvatar, ListItemText,
    Dialog, AppBar, Toolbar, DialogContent, DialogActions,
    Paper,
    CardContent, Button, Tab,
    Tooltip, Input,
    Tabs, TextField, InputAdornment, Icon, Avatar, Typography, Card, Select, MenuItem, InputLabel, FormControl, Divider, CardActions, IconButton
} from '@material-ui/core';
import { useTheme } from '@material-ui/styles';
import { Link } from 'react-router-dom';
import { useForm } from '@fuse/hooks';

function KisiDetail(props) {
    const theme = useTheme();

    const dispatch = useDispatch();
    const kisi = useSelector(({ satisApp }) => satisApp.kisi.data);
    const {form, handleChange, setForm} = useForm(null)

    useEffect(() => {
        function getKisiDetail(mid, kid) {
            dispatch(Actions.getKisi(mid, kid))
        }
        getKisiDetail(props.match.params.mid, props.match.params.kid)
        return function cleanup() {
            dispatch(Actions.clearKisi())
        }
    }, [])

    useEffect(() => {
        if ((kisi && !form) ||
        (kisi && form && kisi._id !== form._id)) {
            setForm(kisi)
        }
    }, [kisi, form, setForm])

    if (!kisi) {
        return <FuseLoading />
    }
    if (!form) {
        return <FuseLoading />
    }


    return (
        <FusePageCarded
            classes={{
                content: "flex",
                header: "min-h-72 h-72 sm:h-136 sm:min-h-136"
            }}
            header={
                <div className="flex flex-1 w-full items-center justify-between">

                    <div className="flex flex-1 flex-col items-center sm:items-start">

                        <FuseAnimate animation="transition.slideRightIn" delay={300}>
                            <Typography className="normal-case flex items-center sm:mb-12" component={Link} role="button" to="/apps/e-commerce/orders" color="inherit">
                                <Icon className="text-20">{theme.direction === "ltr" ? "arrow_back" : "arrow_forward"}</Icon>
                                <span className="mx-4">Kisi Detay</span>
                            </Typography>
                        </FuseAnimate>

                        <div className="flex flex-col min-w-0 items-center sm:items-start">

                            <FuseAnimate animation="transition.slideLeftIn" delay={300}>
                                <Typography className="text-16 sm:text-20 truncate">
                                    {kisi.Adi}
                                </Typography>
                            </FuseAnimate>

                            <FuseAnimate animation="transition.slideLeftIn" delay={300}>
                                <Typography variant="caption">
                                    {kisi.Musteri.Markasi}
                                </Typography>
                            </FuseAnimate>
                        </div>

                    </div>
                </div>
            }
            content={
                <div className="p-16 sm:p-24 max-w-2xl w-full">
                    <div className="pb-48">
                        <div className="pb-16 flex items-center">
                            <Icon color="action">account_circle</Icon>
                            <Typography className="h2 mx-16" color="textSecondary">Kisi</Typography>
                        </div>
                        <TextField
                            className="mt-8 mb-16"
                            error={form.Adi === ''}
                            label="Adi"
                            id="Adi"
                            name="Adi"
                            value={form.Adi}
                            onChange={handleChange}
                            variant="outlined"
                            fullWidth
                        />
                        <TextField
                            className="mt-8 mb-16"
                            error={form.Soyadi === ''}
                            label="Soyadi"
                            id="Soyadi"
                            name="Soyadi"
                            value={form.Soyadi}
                            onChange={handleChange}
                            variant="outlined"
                            fullWidth
                        />
                        <TextField
                            className="mt-8 mb-16"
                            error={form.Unvani === ''}
                            label="Unvani"
                            id="Unvani"
                            name="Unvani"
                            value={form.Unvani}
                            onChange={handleChange}
                            variant="outlined"
                            fullWidth
                        />

                    </div>
                </div>
            }
        />
    )
}

export default withReducer('satisApp', reducer)(KisiDetail);
