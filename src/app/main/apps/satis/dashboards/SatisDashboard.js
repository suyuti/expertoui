import React, {useEffect, useRef} from 'react'
import {FuseAnimateGroup, FusePageSimple, FuseLoading} from '@fuse';
import Widget1 from './widgets/Widget1';
import Board from '../../satisfirsat/board/Board';
import {useDispatch, useSelector} from 'react-redux';
import * as Actions from '../store/actions'
import SatisReducer  from '../store/reducers';
import withReducer from 'app/store/withReducer';
import { Typography} from '@material-ui/core';

function SatisDashboard(props) {
    const dispatch = useDispatch();
    const pageLayout = useRef(null);
    const musteriler = useSelector(({satisApp}) => satisApp.musteriler.data);

    useEffect(() => {
        function getInitials() {
            dispatch(Actions.getMusteriler())
        }
        getInitials()
    }, [])

    if (!musteriler) {
        return <FuseLoading />
    }

    return(
        <React.Fragment>
            <FusePageSimple
                classes={{

                }}
                content= {
                    <div className="p-12">
                        <Typography>Satış Masası</Typography>
                        <div className="widget flex w-full sm:w-1/2 md:w-1/4 p-12">
                            <Widget1 title='Test' data='123'/>
                        </div>
                        <div>
                        </div>
                    </div>
                }
                ref={pageLayout}
            />
        </React.Fragment>
    )
}

export default withReducer('satisApp', SatisReducer)(SatisDashboard);
