import React, { useEffect, useState } from 'react';
import { Button, Tab, Tabs, TextField, InputAdornment, Icon, Typography, Select, MenuItem, InputLabel, FormControl, Divider } from '@material-ui/core';
import { orange } from '@material-ui/core/colors';
import { makeStyles, useTheme } from '@material-ui/styles';
import { FuseAnimate, FusePageCarded, FuseChipSelect, FuseUtils, FuseLoading } from '@fuse';
import { useForm } from '@fuse/hooks';
import { Link } from 'react-router-dom';
import clsx from 'clsx';
import _ from '@lodash';
import { useDispatch, useSelector } from 'react-redux';
import withReducer from 'app/store/withReducer';
import * as Actions from '../store/actions';
import reducer from '../store/reducers';
import FirmaBilgiler from './sections/FirmaBilgiler'
import FirmaRakamlar from './sections/FirmaRakamlar';
import FirmaIletisim from './sections/FirmaIletisim';
import FirmaDestekler from './sections/FirmaDestekler';
import FirmaGorusmeler from './sections/FirmaGorusmeler';
import FirmaSozlesmeler from './sections/FirmaSozlesmeler';
import FirmaTeklifler from './sections/FirmaTeklifler';

const useStyles = makeStyles(theme => ({
    formControl: {
        //margin: theme.spacing(2),
        //padding: theme.spacing(2),
        width: '100%'
    },
    musteriImageFeaturedStar: {
        position: 'absolute',
        top: 0,
        right: 0,
        color: orange[400],
        opacity: 0
    },
    musteriImageUpload: {
        transitionProperty: 'box-shadow',
        transitionDuration: theme.transitions.duration.short,
        transitionTimingFunction: theme.transitions.easing.easeInOut,
    },
    musteriImageItem: {
        transitionProperty: 'box-shadow',
        transitionDuration: theme.transitions.duration.short,
        transitionTimingFunction: theme.transitions.easing.easeInOut,
        '&:hover': {
            '& $musteriImageFeaturedStar': {
                opacity: .8
            }
        },
        '&.featured': {
            pointerEvents: 'none',
            boxShadow: theme.shadows[3],
            '& $musteriImageFeaturedStar': {
                opacity: 1
            },
            '&:hover $musteriImageFeaturedStar': {
                opacity: 1
            }
        }
    }
}));


function Musteri(props) {
    const dispatch = useDispatch();
    const musteri = useSelector(({ satisApp }) => satisApp.musteri);
    const sektorler = useSelector(({ satisApp }) => satisApp.sektorler);
    const musteriKaynaklari = useSelector(({ satisApp }) => satisApp.musteriKaynak);
    const theme = useTheme();

    const classes = useStyles(props);
    const [tabValue, setTabValue] = useState(0);
    const [yeniMusteri, setYeniMusteri] = useState(false);
    const { form, handleChange, setForm } = useForm(null);

    function handleSektorChange(e) {
    }
    function handleMusteriKaynakChange(e) {
    }

    useEffect(() => {
        function getInitialValues() {
            dispatch(Actions.getSektorler())
            dispatch(Actions.getMusteriKaynakList())
        }
        function updateMusteriState() {
            const params = props.match.params;
            const { musteriId } = params;

            if (musteriId === 'new') {
                setYeniMusteri(true)
                dispatch(Actions.newMusteri());
            }
            else {
                dispatch(Actions.getMusteri(props.match.params));
            }
        }

        updateMusteriState();
        getInitialValues()
    }, [dispatch, props.match.params]);


    useEffect(() => {
        if (
            (musteri.data && !form) ||
            (musteri.data && form && musteri.data.id !== form.id)
        ) {
            setForm(musteri.data);
        }
    }, [form, musteri.data, setForm]);

    function handleChangeTab(event, tabValue) {
        setTabValue(tabValue);
    }

    function handleChipChange(value, name) {
        setForm(_.set({ ...form }, name, value.map(item => item.value)));
    }

    function setFeaturedImage(id) {
        setForm(_.set({ ...form }, 'featuredImageId', id));
    }

    function handleUploadChange(e) {
        const file = e.target.files[0];
        if (!file) {
            return;
        }
        const reader = new FileReader();
        reader.readAsBinaryString(file);

        reader.onload = () => {
            setForm(_.set({ ...form }, `images`,
                [
                    {
                        'id': FuseUtils.generateGUID(),
                        'url': `data:${file.type};base64,${btoa(reader.result)}`,
                        'type': 'image'
                    },
                    ...form.images
                ]
            ));
        };

        reader.onerror = function () {
            console.log("error on load image");
        };
    }

    function canBeSubmitted() {
        return (true
            //form.name.length > 0 &&
            //!_.isEqual(musteri.data, form)
        );
    }


    if ((!musteri.data || (musteri.data && props.match.params.musteriId !== musteri.data._id)) && props.match.params.musteriId !== 'new') {
        return <FuseLoading />;
    }

    //if (!sektorler.data) {
    //    return <FuseLoading />;
    //}

    //console.log(sektorler)
    return (
        <FusePageCarded
            classes={{
                toolbar: "p-0",
                header: "min-h-72 h-72 sm:h-136 sm:min-h-136"
            }}
            header={
                form && (
                    <div className="flex flex-1 w-full items-center justify-between">

                        <div className="flex flex-col items-start max-w-full">

                            <FuseAnimate animation="transition.slideRightIn" delay={300}>
                                <Typography className="normal-case flex items-center sm:mb-12" component={Link} role="button" to="/apps/satis/musteriler" color="inherit">
                                    <Icon className="text-20">{theme.direction === "ltr" ? "arrow_back" : "arrow_forward"}</Icon>
                                    <span className="mx-4">Musteriler</span>
                                </Typography>
                            </FuseAnimate>

                            <div className="flex items-center max-w-full">
                                {/*<FuseAnimate animation="transition.expandIn" delay={300}>
                                    {form.images.length > 0 && form.featuredImageId ? (
                                        <img className="w-32 sm:w-48 rounded" src={_.find(form.images, { id: form.featuredImageId }).url} alt={form.name} />
                                    ) : (
                                            <img className="w-32 sm:w-48 rounded" src="assets/images/ecommerce/musteri-image-placeholder.png" alt={form.name} />
                                        )}
                                </FuseAnimate>
                                <div className="flex flex-col min-w-0 mx-8 sm:mc-16">
                                    <FuseAnimate animation="transition.slideLeftIn" delay={300}>
                                        <Typography className="text-16 sm:text-20 truncate">
                                            {form.name ? form.name : 'Yeni Müşteri'}
                                        </Typography>
                                    </FuseAnimate>
                                    <FuseAnimate animation="transition.slideLeftIn" delay={300}>
                                        <Typography variant="caption">Musteri Detay</Typography>
                                    </FuseAnimate>
                                    </div>*/}
                            </div>
                        </div>
                        <FuseAnimate animation="transition.slideRightIn" delay={300}>
                            <Button
                                className="whitespace-no-wrap normal-case"
                                variant="contained"
                                color="secondary"
                                disabled={!canBeSubmitted()}
                                onClick={() => dispatch(Actions.saveMusteri(form))}
                            >
                                Save
                            </Button>
                        </FuseAnimate>
                    </div>
                )
            }
            content={
                form && (
                    <div className="p-16 sm:p-24 max-w-2xl">
                        <FirmaBilgiler form={form} handleChange={handleChange} {...props} />
                        <FirmaIletisim form={form} {...props} />
                        <FirmaRakamlar form={form} {...props} />
                        {!yeniMusteri &&  <FirmaDestekler form={form} {...props} />}
                        {!yeniMusteri &&  <FirmaSozlesmeler form={form} {...props} />}
                        {!yeniMusteri &&  <FirmaTeklifler form={form} {...props} />}
                        {!yeniMusteri &&  <FirmaGorusmeler form={form} {...props} />}
                    </div>
               )
            }
            innerScroll
        />
    )
}

export default withReducer('satisApp', reducer)(Musteri);
