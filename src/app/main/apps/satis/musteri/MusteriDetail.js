import React, {useRef, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import withReducer from 'app/store/withReducer';
import { FusePageCarded, FuseLoading, FuseAnimate } from '@fuse';
import * as Actions from '../store/actions';
import reducer from '../store/reducers';
import FirmaBilgiler from './sections/FirmaBilgiler'
import FirmaIletisim from './sections/FirmaIletisim';
import FirmaRakamlar from './sections/FirmaRakamlar';
import { makeStyles, useTheme } from '@material-ui/styles';
import { Button, Tab, Tabs, TextField, InputAdornment, Icon, Typography, Select, MenuItem, InputLabel, FormControl, Divider } from '@material-ui/core';
import { Link } from 'react-router-dom';
import {useForm} from '@fuse/hooks';
import FirmaSozlesmeler from './sections/FirmaSozlesmeler';
import FirmaGorusmeler from './sections/FirmaGorusmeler'
import FirmaKisiler from './sections/FirmaKisiler';
import * as Permission from 'app/main/helpers/Permission'

const scrollToRef = (ref) => ref.scrollIntoView({behaviour: 'smooth'})   

function MusteriDetail(props) {
    const dispatch  = useDispatch();
    const musteri   = useSelector(({ satisApp }) => satisApp.musteri);
    const theme     = useTheme();
    const {form, handleChange, setForm} = useForm(null);
    const user = useSelector(({auth}) => auth.user);

    const firmaBilgilerRef = useRef(null)
    const firmaIletisimRef = useRef(null)
    const firmaGorusmelerRef = useRef(null)
    const firmaKisilerRef = useRef(null)
    const firmaRakamlarRef = useRef(null)
    const firmaSozlesmelerRef = useRef(null)

    
    useEffect(() => {
        function getMusteri() {
            dispatch(Actions.getMusteri(props.match.params))
        }
        getMusteri()
    }, [dispatch, props.match.params])

    useEffect(() => {
        if ((musteri.data && !form) || 
            (musteri.data && form && musteri.data._id !== form._id)) {
                setForm(musteri.data)
            }
    }, [musteri.data, form, setForm])

    if (!musteri.data) {
        return <FuseLoading />
    }
    if (!form) {
        return <FuseLoading />
    }

    if (musteri.data._id !== props.match.params.musteriId) {
        return <FuseLoading />
    }
    if (form._id !== props.match.params.musteriId) {
        return <FuseLoading />
    }
    console.log(form)
    return (
        <FusePageCarded
            classes={{
                toolbar: "p-0",
                header: "min-h-72 h-72 sm:h-136 sm:min-h-136"
            }}
            header={(
                <div className="flex flex-1 w-full items-center justify-between">
                    <div className="flex flex-col items-start max-w-full">
                        <FuseAnimate animation="transition.slideRightIn" delay={300}>
                            <Typography 
                                className="normal-case flex items-center sm:mb-12" 
                                component={Link} 
                                role="button" 
                                to="/apps/satis/musteriler" 
                                color="inherit">
                                <Icon className="text-20">{theme.direction === "ltr" ? "arrow_back" : "arrow_forward"}</Icon>
                                <span className="mx-4">Musteriler</span>
                            </Typography>
                        </FuseAnimate>
                        <div className="flex items-center max-w-full">
                            <div className="flex flex-col min-w-0 mx-8 sm:mc-16">
                                <FuseAnimate animation="transition.slideLeftIn" delay={300}>
                                    <Typography className="text-16 sm:text-20 truncate">
                                        {musteri.data.Markasi}
                                    </Typography>
                                </FuseAnimate>
                                <FuseAnimate animation="transition.slideLeftIn" delay={300}>
                                    <Typography variant="caption">Musteri Detay</Typography>
                                </FuseAnimate>
                            </div>                        
                            <div className="flex flex-col min-w-0 mx-8 sm:mc-16">
                                <Typography variant="caption">{musteri.data.MusteriStatus}</Typography>
                            </div>
                        </div>
                    </div>
                    <FuseAnimate animation="transition.slideRightIn" delay={300}>
                            <Button
                                className="whitespace-no-wrap normal-case"
                                variant="contained"
                                color="secondary"
                                disabled={!Permission.can(user.role, Permission.MUSTERI_DEGISTIRIR)}

                                //disabled={!canBeSubmitted()}
                                onClick={() => {
                                    dispatch(Actions.updateMusteri(form))
                                    //props.history.goBack()
                                }}
                            >
                                Kaydet
                            </Button>
                        </FuseAnimate>

                </div>
            )}
            content={(
                <>
                <div className="p-16 sm:p-24 max-w-2xl">
                    <FirmaBilgiler firma={form} handleChange={handleChange} {...props} ref={firmaBilgilerRef}/>
                    <Divider />
                    <FirmaIletisim firma={musteri.data} {...props} ref={firmaIletisimRef}/>
                    <Divider />
                    <FirmaKisiler firma={form} {...props} ref={firmaKisilerRef}/>
                    <Divider />
                    <FirmaRakamlar firma={form} handleChange={handleChange} {...props} ref={firmaRakamlarRef} />
                    <Divider />
                    <FirmaGorusmeler firma={form} {...props} ref={firmaGorusmelerRef}/>
                    <Divider />
                    <FirmaSozlesmeler firma={form} {...props} ref={firmaSozlesmelerRef}/>

                </div>
                </>
            )}
            innerScroll
        />
    )
}

export default withReducer('satisApp', reducer)(MusteriDetail);
