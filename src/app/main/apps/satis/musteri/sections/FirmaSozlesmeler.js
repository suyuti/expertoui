import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Button, Tab, Tabs, TextField, InputAdornment, Icon, Typography, Select, MenuItem, InputLabel, FormControl, Divider } from '@material-ui/core';
import * as Actions from '../../store/actions';

function FirmaSozlesmeler(props) {
    const dispatch  = useDispatch();
    var {firma} = props

    useEffect(() => {
        function getSozlesmeler() {
            dispatch(Actions.getMusteriSozlesmeler(firma._id))
        }
        getSozlesmeler()
    }, [])

    return (
        <div className="p-16 sm:p-24 max-w-2xl">
            <div className="pb-48">
                <div className="pb-16 flex items-center">
                    <Icon color="action">access_time</Icon>
                    <Typography className="h2 mx-16" color="textSecondary">Sözleşmeler</Typography>
                </div>
                <div className="table-responsive">
                    <table className="simple">
                        <thead>
                            <tr>
                                <th>Ürün</th>
                                <th>Tarih</th>
                                <th>Adı</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    )
}
export default FirmaSozlesmeler