import React, { useEffect, useState } from 'react'
import { Button, Tab, Tabs, TextField, InputAdornment, Icon, Typography, Select, MenuItem, InputLabel, FormControl, Divider } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import * as Actions from '../../store/actions';
import { makeStyles, useTheme } from '@material-ui/styles';
import { orange } from '@material-ui/core/colors';
import { FuseLoading } from '@fuse';
import FirmaMusteriKaynak from './FirmaKaynak';

const useStyles = makeStyles(theme => ({
    formControl: {
        //margin: theme.spacing(2),
        //padding: theme.spacing(2),
        width: '100%'
    },
    resize: {
        fontSize: 26
    },
}));

function FirmaBilgiler(props) {
    var { firma, handleChange, handleSektorChange, handleMusteriKaynakChange } = props

    var musteriKaynakTurleri = ["Reklam", "Mail", "İç Personel Yönlendirme", "Partner", "Referans", "Web", "Fuar"]

    const dispatch = useDispatch();
    //const musteri = useSelector(({ satisApp }) => satisApp.musteri);
    const sektorler = useSelector(({ satisApp }) => satisApp.sektorler);
    const musteriKaynaklari = useSelector(({ satisApp }) => satisApp.musteriKaynak);
    const [update, setupdate] = useState(false)
    const [kaynak, setKaynak] = useState('')


    const classes = useStyles(props);

    useEffect(() => {
        function getInitialValues() {
            dispatch(Actions.getSektorler())
            dispatch(Actions.getMusteriKaynakList())
        }
        getInitialValues()
    }, [dispatch, props.match.params]);

    function handleMusteriKaynakChanged(e) {
        setKaynak(e.target.value)
    }


    if (!firma) {
        return <FuseLoading />
    }

    return (
        <div className="p-16 sm:p-24 max-w-2xl">
            <div className="pb-48">
                <div className="pb-16 flex items-center">
                    <Icon color="action">access_time</Icon>
                    <Typography className="h2 mx-16" color="textSecondary">Firma Bilgileri</Typography>
                </div>
                <TextField
                    className="mt-8 mb-16"
                    error={firma.Markasi === ''}
                    label="Firma Markası"
                    id="Markasi"
                    name="Markasi"
                    value={firma.Markasi}
                    onChange={handleChange}
                    variant="outlined"
                    fullWidth
                />
                <TextField
                    className="mt-8 mb-16"
                    error={firma.Unvani === ''}
                    label="Firma Ünvanı"
                    id="Unvani"
                    name="Unvani"
                    value={firma.Unvani}
                    onChange={handleChange}
                    variant="outlined"
                    fullWidth
                />
                <div className="flex">

                    <TextField
                        className="mt-8 mb-16 w-1/2"
                        error={firma.Unvani === ''}
                        label="Sektor"
                        id="Sektoru"
                        name="Sektoru"
                        value={firma.Sektoru}
                        onChange={handleChange}
                        variant="outlined"
                        select
                    >
                        {sektorler.data && sektorler.data.map(sektor => (
                            <MenuItem key={sektor._id} value={sektor._id}>{sektor.Adi}</MenuItem>
                        ))}
                    </TextField>
                    <TextField
                        className="mt-8 mb-16 w-1/2"
                        error={firma.Unvani === ''}
                        label="Müşteri Kaynağı"
                        id="MusteriKaynagi"
                        name="MusteriKaynagi"
                        value={firma.MusteriKaynagi}
                        onChange={handleMusteriKaynakChanged}
                        variant="outlined"
                        select
                    >
                        {musteriKaynakTurleri.map(kaynak => (
                            <MenuItem key={kaynak} value={kaynak}>{kaynak}</MenuItem>
                        ))}
                    </TextField>
                </div>
                <FirmaMusteriKaynak kaynakTuru={kaynak} />
            </div>
        </div>

    )
}
export default FirmaBilgiler