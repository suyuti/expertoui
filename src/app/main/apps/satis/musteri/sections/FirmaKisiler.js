import React, { useState, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import {
    List, ListItem, ListItemAvatar, ListItemText,
    Dialog, AppBar, Toolbar, DialogContent, DialogActions,
    Paper,
    CardContent, Button, Tab,
    Tooltip, Input,
    Tabs, TextField, InputAdornment, Icon, Avatar, Typography, Card, Select, MenuItem, InputLabel, FormControl, Divider, CardActions, IconButton
} from '@material-ui/core';
import ImageIcon from '@material-ui/icons/Image';
import * as Actions from '../../store/actions';
import withReducer from 'app/store/withReducer';
import reducer from '../../store/reducers';
import MaskedInput from 'react-text-mask'
import emailMask from 'text-mask-addons/dist/emailMask'
import { useForm } from '@fuse/hooks';
import { FusePageCarded, FuseLoading, FuseAnimate } from '@fuse';
import { makeStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';


const useStyles = makeStyles(theme => ({
    root: {
        width: '100%',
        maxWidth: 360,
        backgroundColor: theme.palette.background.paper,
    },
    inline: {
        display: 'inline',
    },
    card: {
        //        maxHeight: 400,
        //        overflowY: 'auto'
    },
    cardContent: {
        maxHeight: 400,
        overflowY: 'auto'
    }
}));

function FirmaKisiler(props) {
    const dispatch = useDispatch();
    const { firma } = props
    const classes = useStyles();
    const kisiler = useSelector(({ satisApp }) => satisApp.kisiler.data);
    const [filteredData, setFilteredData] = useState(null);
    const [teknikFilteredData, setTeknikFilteredData] = useState(null);
    const [maliFilteredData, setMaliFilteredData] = useState(null);

    useEffect(() => {
        function getFirmaKisiler() {
            dispatch(Actions.getMusteriKisiler(firma._id))
        }
        getFirmaKisiler()


        return function cleanup() {
            dispatch(Actions.clearKisiler())
        }
    }, [])

    useEffect(() => {

        function getFilteredData(kisiler, filtre) {
            return kisiler.filter(k => k.Yetki === filtre)
        }

        if (kisiler) {
            setFilteredData(getFilteredData(kisiler, "Yönetim Yetkilisi"))
            setTeknikFilteredData(getFilteredData(kisiler, "Teknik Yetkilisi"))
            setMaliFilteredData(getFilteredData(kisiler, "Mali Yetkilisi"))
        }
    }, [kisiler])


    return (
        <React.Fragment>
            <div className="flex flex-col p-16">
                <div className="flex items-center">
                    <Icon color="action">access_time</Icon>
                    <Typography className="h2 mx-16" color="textSecondary">Kisiler</Typography>
                </div>
                <div className="flex">
                    <div className="flex flex-col flex-1 items-center">
                        <Typography className="text-gray-900 font-semibold text-xl">Yonetim</Typography>
                        {!filteredData ?
                            <FuseLoading /> :
                            filteredData.length === 0 ? <Typography className="mt-16 text-16 text-gray-500">Kayıt yok</Typography> :
                                <List className="">
                                    {filteredData.map((k, i) => (
                                        <ListItem alignItems="flex-start" className="hover:border-2">
                                            <ListItemAvatar>
                                                <Avatar alt={k.Adi}></Avatar>
                                            </ListItemAvatar>
                                            <ListItemText
                                                primary={
                                                    <Typography
                                                        onClick={() => { props.history.push(`/apps/satis/kisi/${firma._id}/${k._id}`) }}
                                                        className="text-xl text-black cursor-pointer"
                                                    >{k.Adi}</Typography>
                                                }
                                                secondary={
                                                    <React.Fragment>
                                                        <Typography
                                                            component="span"
                                                            variant="body2"
                                                            className={classes.inline}
                                                            color="textPrimary"
                                                        >
                                                            {k.Musteri.Markasi}
                                                        </Typography>
                                                        -- {k.Yetki}
                                                    </React.Fragment>
                                                }
                                            ></ListItemText>
                                        </ListItem>
                                    ))}
                                </List>
                        }
                    </div>
                    <div className="flex flex-col border-l-1 flex-1 items-center">
                        <Typography className="text-gray-900 font-semibold text-xl">Teknik</Typography>
                        {!teknikFilteredData ?
                            <FuseLoading /> :
                            teknikFilteredData.length === 0 ? <Typography className="mt-16 text-16 text-gray-500">Kayıt yok</Typography> :

                                <List className="">
                                    {teknikFilteredData.map((k, i) => (
                                        <ListItem alignItems="flex-start">
                                            <ListItemAvatar>
                                                <Avatar alt={k.Adi}></Avatar>
                                            </ListItemAvatar>
                                            <ListItemText
                                                primary={
                                                    <Typography
                                                        onClick={() => { props.history.push(`/apps/satis/kisi/${firma._id}/${k._id}`) }}
                                                        className="text-xl text-black cursor-pointer"
                                                    >{k.Adi}</Typography>
                                                }
                                                secondary={
                                                    <React.Fragment>
                                                        <Typography
                                                            component="span"
                                                            variant="body2"
                                                            className={classes.inline}
                                                            color="textPrimary"
                                                        >
                                                            {k.Musteri.Markasi}
                                                        </Typography>
                                                        -- {k.Yetki}
                                                    </React.Fragment>
                                                }
                                            ></ListItemText>
                                        </ListItem>
                                    ))}
                                </List>
                        }
                    </div>
                    <div className="flex flex-col border-l-1 flex-1 items-center">
                        <Typography className="text-gray-900 font-semibold text-xl ">Mali</Typography>
                        {!maliFilteredData ?
                            <FuseLoading /> :
                            maliFilteredData.length === 0 ? <Typography className="items-center mt-16 text-16 text-gray-500">Kayıt yok</Typography> :
                                <List className="">
                                    {maliFilteredData.map((k, i) => (
                                        <ListItem alignItems="flex-start">
                                            <ListItemAvatar>
                                                <Avatar alt={k.Adi}></Avatar>
                                            </ListItemAvatar>
                                            <ListItemText
                                                primary={<Typography
                                                    onClick={() => { props.history.push(`/apps/satis/kisi/${firma._id}/${k._id}`) }}
                                                    className="text-xl text-black cursor-pointer"
                                                >{k.Adi}</Typography>
                                                }
                                                secondary={
                                                    <React.Fragment>
                                                        <Typography
                                                            component="span"
                                                            variant="body2"
                                                            className={classes.inline}
                                                            color="textPrimary"
                                                        >
                                                            {k.Musteri.Markasi}
                                                        </Typography>
                                                        -- {k.Yetki}
                                                    </React.Fragment>
                                                }
                                            ></ListItemText>
                                        </ListItem>
                                    ))}
                                </List>}                    </div>

                </div>
            </div>
            {/*
            <div className="p-16 sm:p-24 max-w-2xl flex">
                    <div className="pb-16 flex items-center justify-between">
                        <div>
                            <Icon color="action">access_time</Icon>
                            <Typography className="h2 mx-16" color="textSecondary">Kisiler</Typography>
                        </div>
                        <div>
                            <IconButton>
                                <Icon fontSize="large">add_circle_outline</Icon>
                            </IconButton>
                        </div>
                    </div>
                    <div className="flex">
                        <div className="flex  flex-col flex-1">
                            <Typography>Yonetim</Typography>
                            <List className="w-1/3">
                            {kisiler.map((k, i) => (
                                <>
                                    <ListItem alignItems="flex-start">
                                        <ListItemAvatar>
                                            <Avatar alt={k.Adi} src="/static/images/avatar/1.jpg" />
                                        </ListItemAvatar>
                                        <ListItemText
                                            primary={k.Adi}
                                            secondary={
                                                <React.Fragment>
                                                    <Typography
                                                        component="span"
                                                        variant="body2"
                                                        className={classes.inline}
                                                        color="textPrimary"
                                                    >
                                                        {k.Musteri.Markasi}
                                                    </Typography>
                                                    -- {k.Yetki}
                                                </React.Fragment>
                                            }
                                        />
                                    </ListItem>
                                    <Divider variant="inset" component="li" />
                                </>
                            ))}
                        </List>
                        </div>
                        <div className="flex  flex-col flex-1">
                            <Typography>Teknik</Typography>
                            <List className="w-1/3">
                            {kisiler.map((k, i) => (
                                <>
                                    <ListItem alignItems="flex-start">
                                        <ListItemAvatar>
                                            <Avatar alt={k.Adi} src="/static/images/avatar/1.jpg" />
                                        </ListItemAvatar>
                                        <ListItemText
                                            primary={k.Adi}
                                            secondary={
                                                <React.Fragment>
                                                    <Typography
                                                        component="span"
                                                        variant="body2"
                                                        className={classes.inline}
                                                        color="textPrimary"
                                                    >
                                                        {k.Musteri.Markasi}
                                                    </Typography>
                                                    -- {k.Yetki}
                                                </React.Fragment>
                                            }
                                        />
                                    </ListItem>
                                    <Divider variant="inset" component="li" />
                                </>
                            ))}
                        </List>
                        </div>
                        <div className="flex flex-col flex-1">
                            <Typography>Mali</Typography>
                            <List className="w-1/3">
                            {kisiler.map((k, i) => (
                                <>
                                    <ListItem alignItems="flex-start">
                                        <ListItemAvatar>
                                            <Avatar alt={k.Adi} src="/static/images/avatar/1.jpg" />
                                        </ListItemAvatar>
                                        <ListItemText
                                            primary={k.Adi}
                                            secondary={
                                                <React.Fragment>
                                                    <Typography
                                                        component="span"
                                                        variant="body2"
                                                        className={classes.inline}
                                                        color="textPrimary"
                                                    >
                                                        {k.Musteri.Markasi}
                                                    </Typography>
                                                    -- {k.Yetki}
                                                </React.Fragment>
                                            }
                                        />
                                    </ListItem>
                                    <Divider variant="inset" component="li" />
                                </>
                            ))}
                        </List>
                        </div>
                    </div>
            </div>
            */}
        </React.Fragment>
    )
}

export default withReducer('satisApp', reducer)(FirmaKisiler);
