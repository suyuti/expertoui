import React from 'react'
import { TextField, Typography } from '@material-ui/core';
import { KeyboardDatePicker, DateTimePicker, MuiPickersUtilsProvider } from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';

const ReklamKaynak = (props) => (
    <div className="flex flex-col">
        <TextField
            className="mt-8 mb-8"
            //error={firma.Unvani === ''}
            label="Reklam Ortami"
            id="ReklamOrtami"
            name="ReklamOrtami"
            //value={firma.Unvani}
            //onChange={handleChange}
            variant="outlined"
            fullWidth
        />
        <TextField
            className="mt-8 mb-16"
            //error={firma.Unvani === ''}
            label="Reklam Aciklama"
            id="ReklamAciklama"
            name="ReklamAciklama"
            multiline
            rows={3}
            //value={firma.Unvani}
            //onChange={handleChange}
            variant="outlined"
            fullWidth
        />
    </div>
)

const MailKaynak = (props) => {
    const [selectedDate, setSelectedDate] = React.useState(new Date('2014-08-18T21:11:54'));

    const handleDateChange = date => {
        setSelectedDate(date);
    };
    return (
        <div className="flex flex-col">
            <div className="flex">
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                    <KeyboardDatePicker
                        className="mt-8 mb-8"
                        fullWidth
                        disableToolbar
                        variant="outlined"
                        format="dd/MM/yyyy"
                        margin="normal"
                        id="date-picker-inline"
                        label="Mail Tarihi"
                        value={selectedDate}
                        onChange={handleDateChange}
                        KeyboardButtonProps={{
                            'aria-label': 'change date',
                        }}
                    />
                </MuiPickersUtilsProvider>
                <TextField
                    className="mt-8 mb-8 ml-8"
                    //error={firma.Unvani === ''}
                    label="Mail Baslik"
                    id="Unvani"
                    name="Unvani"
                    //value={firma.Unvani}
                    //onChange={handleChange}
                    variant="outlined"
                    fullWidth
                />
            </div>
            <TextField
                className="mt-8 mb-16"
                //error={firma.Unvani === ''}
                label="Aciklama"
                id="Unvani"
                name="Unvani"
                //value={firma.Unvani}
                //onChange={handleChange}
                variant="outlined"
                fullWidth
                multiline
                rows={3}
            />
        </div>
    )
}
const IcPersonelYonlendirmeKaynak = (props) => {
    return (
        <div>
            <TextField
                className="mt-8 mb-16"
                //error={firma.Unvani === ''}
                label="Personel"
                id="Unvani"
                name="Unvani"
                //value={firma.Unvani}
                //onChange={handleChange}
                variant="outlined"
                fullWidth
            />
            <TextField
                className="mt-8 mb-16"
                //error={firma.Unvani === ''}
                label="Aciklama"
                id="Unvani"
                name="Unvani"
                //value={firma.Unvani}
                //onChange={handleChange}
                variant="outlined"
                fullWidth
                multiline
                rows={3}
            />
        </div>
    )
}

const PartnerKaynak = (props) => (
    <div>
        <TextField
            className="mt-8 mb-16"
            //error={firma.Unvani === ''}
            label="Partner"
            id="Unvani"
            name="Unvani"
            //value={firma.Unvani}
            //onChange={handleChange}
            variant="outlined"
            fullWidth
        />
        <TextField
            className="mt-8 mb-16"
            //error={firma.Unvani === ''}
            label="Tarih"
            id="Unvani"
            name="Unvani"
            //value={firma.Unvani}
            //onChange={handleChange}
            variant="outlined"
            fullWidth
        />
        <TextField
            className="mt-8 mb-16"
            //error={firma.Unvani === ''}
            label="Oran"
            id="Unvani"
            name="Unvani"
            //value={firma.Unvani}
            //onChange={handleChange}
            variant="outlined"
            fullWidth
        />
        <TextField
            className="mt-8 mb-16"
            //error={firma.Unvani === ''}
            label="Aciklama"
            id="Unvani"
            name="Unvani"
            //value={firma.Unvani}
            //onChange={handleChange}
            variant="outlined"
            fullWidth
            multiline
            rows={3}
        />
    </div>
)

const ReferansKaynak = (props) => (
    <div>
        <TextField
            className="mt-8 mb-16"
            //error={firma.Unvani === ''}
            label="Musteri Ortami"
            id="Unvani"
            name="Unvani"
            //value={firma.Unvani}
            //onChange={handleChange}
            variant="outlined"
            fullWidth
        />
        <TextField
            className="mt-8 mb-16"
            //error={firma.Unvani === ''}
            label="Tarih"
            id="Unvani"
            name="Unvani"
            //value={firma.Unvani}
            //onChange={handleChange}
            variant="outlined"
            fullWidth
        />
        <TextField
            className="mt-8 mb-16"
            //error={firma.Unvani === ''}
            label="Aciklama"
            id="Unvani"
            name="Unvani"
            //value={firma.Unvani}
            //onChange={handleChange}
            variant="outlined"
            fullWidth
            multiline
            rows={3}
        />
    </div>
)

const WebKaynak = (props) => (
    <div>
        <TextField
            className="mt-8 mb-16"
            //error={firma.Unvani === ''}
            label="Tarih"
            id="Unvani"
            name="Unvani"
            //value={firma.Unvani}
            //onChange={handleChange}
            variant="outlined"
            fullWidth
        />
        <TextField
            className="mt-8 mb-16"
            //error={firma.Unvani === ''}
            label="Aciklama"
            id="Unvani"
            name="Unvani"
            //value={firma.Unvani}
            //onChange={handleChange}
            variant="outlined"
            fullWidth
            multiline
            rows={3}
        />
    </div>
)

const FuarKaynak = (props) => (
    <div>
        <TextField
            className="mt-8 mb-16"
            //error={firma.Unvani === ''}
            label="Hangi Fuar"
            id="Unvani"
            name="Unvani"
            //value={firma.Unvani}
            //onChange={handleChange}
            variant="outlined"
            fullWidth
        />
        <TextField
            className="mt-8 mb-16"
            //error={firma.Unvani === ''}
            label="Tarih"
            id="Unvani"
            name="Unvani"
            //value={firma.Unvani}
            //onChange={handleChange}
            variant="outlined"
            fullWidth
        />
        <TextField
            className="mt-8 mb-16"
            //error={firma.Unvani === ''}
            label="Aciklama"
            id="Unvani"
            name="Unvani"
            //value={firma.Unvani}
            //onChange={handleChange}
            variant="outlined"
            fullWidth
            multiline
            rows={3}
        />
    </div>
)

function FirmaMusteriKaynak(props) {
    const { kaynakTuru } = props
    if (kaynakTuru === '') {
        return <></>
    }
    return (
        <React.Fragment>
            <div className="border-solid border rounded shadow flex flex-col p-16">
                <Typography className="capitalize text-600">Müşteri Kaynağı {kaynakTuru}</Typography>
                {kaynakTuru === "Reklam" && (
                    <ReklamKaynak {...props} />
                )}
                {kaynakTuru === "Mail" && (
                    <MailKaynak {...props} />
                )}
                {kaynakTuru === "İç Personel Yönlendirme" && (
                    <IcPersonelYonlendirmeKaynak {...props} />
                )}
                {kaynakTuru === "Partner" && (
                    <PartnerKaynak {...props} />
                )}
                {kaynakTuru === "Referans" && (
                    <ReferansKaynak {...props} />
                )}
                {kaynakTuru === "Web" && (
                    <WebKaynak {...props} />
                )}
                {kaynakTuru === "Fuar" && (
                    <FuarKaynak {...props} />
                )}
            </div>
        </React.Fragment>
    )
}

export default FirmaMusteriKaynak