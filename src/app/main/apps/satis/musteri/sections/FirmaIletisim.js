import React, { useEffect, useState } from 'react'
import {
    Button,
    AppBar,
    Toolbar,
    DialogContent,
    DialogActions,
    Tab, Tabs, TextField, InputAdornment,
    Icon, Typography, Select, MenuItem,
    InputLabel, FormControl, Divider,
    Card, CardHeader, CardContent, IconButton,
    Collapse,
    Dialog, 
    Table,
    TableRow,
    TableHead,
    TableCell,
    TableBody
} from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import * as Actions from '../../store/actions';
import { makeStyles, useTheme } from '@material-ui/styles';
import { orange, red } from '@material-ui/core/colors';
import clsx from 'clsx';
import withReducer from 'app/store/withReducer';
import reducer from '../../store/reducers';

const useStyles = makeStyles(theme => ({
    root: {
        maxWidth: 345,
    },
    media: {
        height: 0,
        paddingTop: '56.25%', // 16:9
    },
    expand: {
        transform: 'rotate(0deg)',
        marginLeft: 'auto',
        transition: theme.transitions.create('transform', {
            duration: theme.transitions.duration.shortest,
        }),
    },
    expandOpen: {
        transform: 'rotate(180deg)',
    },
    avatar: {
        backgroundColor: red[500],
    },
}));

function AdresDlg(props) {
    const dispatch = useDispatch();

    const [adres, setAdres] = useState({ musteriId: props.musteriId })
    function handleChange(e) {
        e.preventDefault()
        var a = { ...adres }
        a[e.target.name] = e.target.value
        setAdres(a)
    }

    function handleSubmit() {
        dispatch(Actions.adresEkle(adres))
    }

    return (
        <Dialog
            open={props.show}
            classes={{
                paper: "m-24"
            }}
            fullWidth
            maxWidth="xs"
        >
            <AppBar position="static" elevation={1}>
                <Toolbar className="flex w-full">
                    <Typography variant="subtitle1" color="inherit">
                        Yeni Adres Ekleme
                    </Typography>
                </Toolbar>
                <div className="flex flex-col items-center justify-center pb-24">
                </div>
            </AppBar>
            <form noValidate
                //onSubmit={handleSubmit} 
                className="flex flex-col md:overflow-hidden">
                <DialogContent classes={{ root: "p-24" }}>
                    <div className="flex">
                        <div className="min-w-48 pt-20">
                            <Icon color="action">account_circle</Icon>
                        </div>

                        <TextField
                            className="mb-24"
                            label="Adres türü"
                            autoFocus
                            id="adterturu"
                            name="Turu"
                            value={adres.Turu}
                            onChange={handleChange}
                            variant="outlined"
                            required
                            fullWidth
                        />
                    </div>
                    <div className="flex">
                        <div className="min-w-48 pt-20">
                            <Icon color="action">account_circle</Icon>
                        </div>
                        <TextField
                            className="mb-24"
                            label="Adres"
                            id="adres"
                            name="Adres"
                            value={adres.Adres}
                            onChange={handleChange}
                            variant="outlined"
                            required
                            fullWidth
                        />
                    </div>

                </DialogContent>

                <DialogActions className="justify-between p-8">
                    <div className="px-16">
                        <Button
                            variant="contained"
                            color="primary"
                            onClick={() => { 
                                dispatch(Actions.adresEkle(adres)) 
                                setAdres({ musteriId: props.musteriId })
                            }}
                        //type="submit"
                        //disabled={!canBeSubmitted()}
                        >Ekle</Button>
                        <Button
                            variant="contained"
                            color="primary"
                            onClick={() => { 
                                dispatch(Actions.closeAdresDlg()) 
                                setAdres({ musteriId: props.musteriId })
                            }}
                        //type="submit"
                        //disabled={!canBeSubmitted()}
                        >Kapat</Button>
                    </div>
                </DialogActions>
            </form>

        </Dialog>
    )
}

function FirmaIletisim(props) {
    var { firma, form, handleChange, handleSektorChange, handleMusteriKaynakChange } = props
    const dispatch = useDispatch();
    const [expandedAdres, setExpandedAdres] = useState(false)
    const classes = useStyles(props);
    const showAdresDlg = useSelector(({ satisApp }) => satisApp.musteri.showAdresDlg);

    useEffect(() => {
        function getAdresler() {
            dispatch(Actions.getMusteriAdresleri(firma._id))
        }
        getAdresler()
    }, [])

    return (
        <React.Fragment>
            <div className="p-16 sm:p-24 max-w-2xl">
                <div className="pb-48">
                    <div className="pb-16 flex items-center">
                        <Icon color="action">phone</Icon>
                        <Typography className="h2 mx-16" color="textSecondary">İletişim</Typography>
                    </div>

                    <Card variant="outlined">
                        <CardHeader
                            title="Adresler"
                            avatar={
                                <IconButton className={clsx(classes.expand, {
                                    [classes.expandOpen]: expandedAdres,
                                })}
                                    onClick={() => setExpandedAdres(!expandedAdres)}
                                >
                                    <Icon fontSize="large" >expand_more</Icon>
                                </IconButton>
                            }
                            action={
                                <IconButton onClick={() => { dispatch(Actions.openAdresDlg()) }}>
                                    <Icon fontSize="large">add_circle_outline</Icon>
                                </IconButton>
                            }
                        >
                        </CardHeader>
                        <Collapse in={expandedAdres} timeout="auto" unmountOnExit>
                            <CardContent>
                                    <Table size="small" >
                                        <TableHead>
                                            <TableRow>
                                                <TableCell>Adres Türü</TableCell>
                                                <TableCell>Adres</TableCell>
                                                <TableCell>İşlem </TableCell>
                                            </TableRow>
                                        </TableHead>
                                        <TableBody>
                                            {firma.Adresler.map((a, i) => (
                                                <TableRow key={a._id}>
                                                    <TableCell>
                                                        {a.AdresTuru}
                                                    </TableCell>
                                                    <TableCell>
                                                        {a.Adres1}
                                                    </TableCell>
                                                    <TableCell>
                                                        <IconButton onClick={() => {
                                                            dispatch(Actions.adresSil(firma._id, a._id))
                                                        }}>
                                                            <Icon color="action">delete_outline</Icon>
                                                        </IconButton>
                                                    </TableCell>
                                                </TableRow>)
                                            )}
                                        </TableBody>
                                    </Table>
                            </CardContent>
                        </Collapse>
                    </Card>
                </div>
            </div>
            <AdresDlg show={showAdresDlg} musteriId={firma._id} />
        </React.Fragment>
    )
}
export default withReducer('satisApp', reducer)(FirmaIletisim);

//export default FirmaIletisim