import React from 'react'
import { Button, Tab, Tabs, TextField, InputAdornment, Icon, Typography, Select, MenuItem, InputLabel, FormControl, Divider } from '@material-ui/core';

function FirmaTeklifler(props) {
    return (
        <div className="p-16 sm:p-24 max-w-2xl">
            <div className="pb-48">
                <div className="pb-16 flex items-center">
                    <Icon color="action">access_time</Icon>
                    <Typography className="h2 mx-16" color="textSecondary">Teklifler</Typography>
                </div>
                <div className="table-responsive">
                    <table className="simple">
                        <thead>
                            <tr>
                                <th>Ürün</th>
                                <th>Tarih</th>
                                <th>Durum</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr key="1">
                                <td>
                                    Merkez
                            </td>
                                <td>
                                    İkitelli Organiza sanayi bölgesi, A Blok 11
                            </td>
                                <tb>
                                </tb>
                            </tr>
                            <tr key="2">
                                <td>
                                    Fabrika
                            </td>
                                <td>
                                    İkitelli Organiza sanayi bölgesi, A Blok 11
                            </td>
                                <tb>
                                </tb>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    )
}
export default FirmaTeklifler