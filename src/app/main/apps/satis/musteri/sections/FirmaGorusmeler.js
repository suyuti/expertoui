import React, { useState, useEffect } from 'react'
import {
    Dialog, AppBar, Toolbar, DialogContent, DialogActions,
    Paper,
    CardContent, Button, Tab,
    Tooltip, Input,
    Tabs, TextField, InputAdornment, Icon, Avatar, Typography, Card, Select, MenuItem, InputLabel, FormControl, Divider, CardActions, IconButton
} from '@material-ui/core';
import Timeline from '../../../../components/timeline/Timeline'
import TimelineItem from '../../../../components/timeline/TimelineItem'
import { makeStyles, useTheme } from '@material-ui/styles';
import { amber, blue, blueGrey, green } from '@material-ui/core/colors';
import { useFlexLayout } from 'react-table';
import moment from 'moment';
import { ThemeProvider } from '@material-ui/styles';
import { useDispatch, useSelector } from 'react-redux';
import { FuseAnimate, FuseLoading } from '@fuse';
import * as Actions from '../../store/actions';
import withReducer from 'app/store/withReducer';
import reducer from '../../store/reducers';


const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
        '& > *': {
            margin: theme.spacing(1),
        },
    },
    small: {
        width: theme.spacing(3),
        height: theme.spacing(3),
    },
    large: {
        width: theme.spacing(7),
        height: theme.spacing(7),
    },

    timelineWrapper: {
        background: theme.palette.grey[100],
        display: 'flex',
        flexDirection: 'column',
        padding: 10,
        border: '1px solid rgba(0, 0, 0, 0.12)',
        //paddingLeft: 20,
        //paddingRight: 10,
        position: 'relative',
        overflowY: 'auto',
        overflowX: 'hidden',
        height: 400,
        /*'&::before': {
            content: `''`,
            height: '100%',
            width: 5,
            background: 'red',
            position: 'absolute'
        }*/
    },

    gorusme: {
        flexGrow: 1,
        display: 'flex',
        flexDirection: 'row',
        marginTop: 10,
        marginLeft: 0,
        position: 'relative',
        paddingLeft: 20,
        paddingRight: 20,
        //left: 30,
        /*'&::before': {
            content: `''`,
            width: 200,
            height: 10,
            backgroundColor: blue[500],
            position: 'absolute',
            top: 0,
            left: 0,
            transform: `rotate(0deg)`
        }*/
    },
    gorusmeHeader: {
        display: `flex`,
        flexShrink: 0,
        alignItems: 'center',
        justifyContent: 'space-between',
        padding: '1.75rem',
        height: '1rem',
        backgroundColor: blue[500],
        color: theme.palette.getContrastText(blue[500]),
        position: 'relative',

    }
}));

function GorusmeDlg(props) {
    const dispatch = useDispatch();
    const [gorusme, setGorusme] = useState({ musteriId: props.musteriId })
    function handleChange(e) {
        e.preventDefault()
        var a = { ...gorusme }
        a[e.target.name] = e.target.value
        setGorusme(a)
    }

    function handleSubmit() {
        dispatch(Actions.musteriGorusmeEkle(props.firmaId, gorusme))
    }
    return (
        <Dialog
            open={props.show}
            classes={{
                paper: "m-24"
            }}
            fullWidth
            maxWidth="xs"
        >
            <AppBar position="static" elevation={1}>
                <Toolbar className="flex w-full">
                    <Typography variant="subtitle1" color="inherit">
                        Yeni Gorusme Ekleme
                    </Typography>
                </Toolbar>
                <div className="flex flex-col items-center justify-center pb-24">
                </div>
            </AppBar>
            <form noValidate
                //onSubmit={handleSubmit} 
                className="flex flex-col md:overflow-hidden">
                <DialogContent classes={{ root: "p-24" }}>
                    <div className="flex">
                        <div className="min-w-48 pt-20">
                            <Icon color="action">account_circle</Icon>
                        </div>

                        <TextField
                            className="mb-24"
                            label="Konu"
                            autoFocus
                            id="Konu"
                            name="Konu"
                            //value={adres.Turu}
                            //onChange={handleChange}
                            variant="outlined"
                            required
                            fullWidth
                        />
                    </div>
                    <div className="flex">
                        <div className="min-w-48 pt-20">
                            <Icon color="action">account_circle</Icon>
                        </div>
                        <TextField
                            multiline
                            rows={3}
                            className="mb-24"
                            label="Açıklama"
                            id="Aciklama"
                            name="Aciklama"
                            //value={adres.Adres}
                            //onChange={handleChange}
                            variant="outlined"
                            required
                            fullWidth
                        />
                    </div>

                </DialogContent>

                <DialogActions className="justify-between p-8">
                    <div className="px-16">
                        <Button
                            variant="contained"
                            color="primary"
                            onClick={() => { 
                                //dispatch(Actions.adresEkle(adres)) 
                                //setAdres({ musteriId: props.musteriId })
                            }}
                        //type="submit"
                        //disabled={!canBeSubmitted()}
                        >Ekle</Button>
                        <Button
                            variant="contained"
                            color="primary"
                            onClick={() => { 
                                dispatch(Actions.hideYeniGorusmeDlg()) 
                                //setAdres({ musteriId: props.musteriId })
                            }}
                        //type="submit"
                        //disabled={!canBeSubmitted()}
                        >Kapat</Button>
                    </div>
                </DialogActions>
            </form>

        </Dialog>
    )
}


function FirmaGorusmeler(props) {
    const dispatch  = useDispatch();
    var {firma} = props

    const mainTheme = useSelector(({ fuse }) => fuse.settings.mainTheme);
    const gorusmeler = useSelector(({ satisApp }) => satisApp.gorusmeler.data);
    const showGorusmeDlg = useSelector(({ satisApp }) => satisApp.gorusmeler.showGorusmeDlg);
    

    useEffect(() => {
        function getGorusmeler() {
            dispatch(Actions.getMusteriGorusmeList(firma._id))
        }
        getGorusmeler()
    }, [])

/*    const gorusmeler = [
        { _id: 1, Kanal: 'Telefon', Konu: 'Goruime 1', GorusmeyiYapanKisi: 'Ahmet', GorusmeYapilanKisi: 'Mehmet', Zaman: '10/03/2020 11:21', Aciklama: 'aciklama 1' },
        { _id: 2, Kanal: 'Mail', Konu: 'Goruime 2', GorusmeyiYapanKisi: 'Ahmet', GorusmeYapilanKisi: 'Mehmet', Zaman: '10/03/2020 11:21', Aciklama: 'aciklama 2' },
        { _id: 4, Kanal: 'Whatsapp', Konu: 'Goruime 4', GorusmeyiYapanKisi: 'Ahmet', GorusmeYapilanKisi: 'Mehmet', Zaman: '11/03/2020 11:21', Aciklama: 'aciklama 4' },
        { _id: 3, Kanal: 'Yüzyüze', Konu: 'Goruime 3', GorusmeyiYapanKisi: 'Ahmet', GorusmeYapilanKisi: 'Mehmet', Zaman: '11/03/2020 11:21', Aciklama: 'aciklama 3' },
        { _id: 1, Kanal: 'Telefon', Konu: 'Goruime 1', GorusmeyiYapanKisi: 'Ahmet', GorusmeYapilanKisi: 'Mehmet', Zaman: '10/03/2020 11:21', Aciklama: 'aciklama 1' },
        { _id: 2, Kanal: 'Mail', Konu: 'Goruime 2', GorusmeyiYapanKisi: 'Ahmet', GorusmeYapilanKisi: 'Mehmet', Zaman: '10/03/2020 11:21', Aciklama: 'aciklama 2' },
        { _id: 4, Kanal: 'Whatsapp', Konu: 'Goruime 4', GorusmeyiYapanKisi: 'Ahmet', GorusmeYapilanKisi: 'Mehmet', Zaman: '11/03/2020 11:21', Aciklama: 'aciklama 4' },
        { _id: 3, Kanal: 'Yüzyüze', Konu: 'Goruime 3', GorusmeyiYapanKisi: 'Ahmet', GorusmeYapilanKisi: 'Mehmet', Zaman: '11/03/2020 11:21', Aciklama: 'aciklama 3' },
        { _id: 1, Kanal: 'Telefon', Konu: 'Goruime 1', GorusmeyiYapanKisi: 'Ahmet', GorusmeYapilanKisi: 'Mehmet', Zaman: '10/03/2020 11:21', Aciklama: 'aciklama 1' },
        { _id: 2, Kanal: 'Mail', Konu: 'Goruime 2', GorusmeyiYapanKisi: 'Ahmet', GorusmeYapilanKisi: 'Mehmet', Zaman: '10/03/2020 11:21', Aciklama: 'aciklama 2' },
        { _id: 4, Kanal: 'Whatsapp', Konu: 'Goruime 4', GorusmeyiYapanKisi: 'Ahmet', GorusmeYapilanKisi: 'Mehmet', Zaman: '11/03/2020 11:21', Aciklama: 'aciklama 4' },
        { _id: 3, Kanal: 'Yüzyüze', Konu: 'Goruime 3', GorusmeyiYapanKisi: 'Ahmet', GorusmeYapilanKisi: 'Mehmet', Zaman: '11/03/2020 11:21', Aciklama: 'aciklama 3' },
        { _id: 1, Kanal: 'Telefon', Konu: 'Goruime 1', GorusmeyiYapanKisi: 'Ahmet', GorusmeYapilanKisi: 'Mehmet', Zaman: '10/03/2020 11:21', Aciklama: 'aciklama 1' },
        { _id: 2, Kanal: 'Mail', Konu: 'Goruime 2', GorusmeyiYapanKisi: 'Ahmet', GorusmeYapilanKisi: 'Mehmet', Zaman: '10/03/2020 11:21', Aciklama: 'aciklama 2' },
        { _id: 4, Kanal: 'Whatsapp', Konu: 'Goruime 4', GorusmeyiYapanKisi: 'Ahmet', GorusmeYapilanKisi: 'Mehmet', Zaman: '11/03/2020 11:21', Aciklama: 'aciklama 4' },
        { _id: 3, Kanal: 'Yüzyüze', Konu: 'Goruime 3', GorusmeyiYapanKisi: 'Ahmet', GorusmeYapilanKisi: 'Mehmet', Zaman: '11/03/2020 11:21', Aciklama: 'aciklama 3' },
        { _id: 1, Kanal: 'Telefon', Konu: 'Goruime 1', GorusmeyiYapanKisi: 'Ahmet', GorusmeYapilanKisi: 'Mehmet', Zaman: '10/03/2020 11:21', Aciklama: 'aciklama 1' },
        { _id: 2, Kanal: 'Mail', Konu: 'Goruime 2', GorusmeyiYapanKisi: 'Ahmet', GorusmeYapilanKisi: 'Mehmet', Zaman: '10/03/2020 11:21', Aciklama: 'aciklama 2' },
        { _id: 4, Kanal: 'Whatsapp', Konu: 'Goruime 4', GorusmeyiYapanKisi: 'Ahmet', GorusmeYapilanKisi: 'Mehmet', Zaman: '11/03/2020 11:21', Aciklama: 'aciklama 4' },
        { _id: 3, Kanal: 'Yüzyüze', Konu: 'Goruime 3', GorusmeyiYapanKisi: 'Ahmet', GorusmeYapilanKisi: 'Mehmet', Zaman: '11/03/2020 11:21', Aciklama: 'aciklama 3' },
    ]
    */
    const theme = useTheme();
    const classes = useStyles();

    if (!gorusmeler) {
        return <FuseLoading />
    }

    var day = null;
    var showDay = false;
    return (
        <div className="p-16 sm:p-24 max-w-2xl">
            <div className="pb-48">
                <div className="flex justify-between">
                    <div className="pb-16 flex items-center">
                        <Icon color="action">access_time</Icon>
                        <Typography className="h2 mx-16" color="textSecondary">Görüşmeler</Typography>
                    </div>
                    <div className="flex flex-row">

                        <div className="flex flex-1 items-center justify-center px-12">

                            <ThemeProvider theme={mainTheme}>
                                <FuseAnimate animation="transition.slideDownIn" delay={300}>
                                    <Paper className="flex items-center w-full max-w-512 px-8 py-4 rounded-8 border-blue-500" elevation={1}>

                                        <Icon color="action">search</Icon>

                                        <Input
                                            placeholder="Search"
                                            className="flex flex-1 mx-8"
                                            disableUnderline
                                            fullWidth
                                            //value={searchText}
                                            inputProps={{
                                                'aria-label': 'Search'
                                            }}
                                        //onChange={ev => dispatch(Actions.setMusterilerSearchText(ev))}
                                        />
                                    </Paper>
                                </FuseAnimate>
                            </ThemeProvider>
                            <IconButton
                                onClick={() => {dispatch(Actions.showYeniGorusmeDlg())}}
                            >
                                <Icon>access_time</Icon>
                            </IconButton>
                        </div>
                    </div>
                </div>
                <div className={classes.timelineWrapper}>
                    {gorusmeler.map((g, i) => {
                        var date = moment(g.Zaman).format('DD/MM/YYYY')
                        if (date === day) {
                            showDay = false;
                        }
                        else {
                            day = date
                            showDay = true;
                        }
                        return (
                            <React.Fragment>
                                <div className="flex flex-col">
                                    {showDay ? <Typography variant="h6" className="m-10 text-orange-500 ">{moment(day).format('DD/MM/YYYY')}</Typography> : ''}
                                    <div className={classes.gorusme}>
                                        <Card elevation={2} className="flex flex-col flex-grow">
                                            <div className={classes.gorusmeHeader}
                                                style={{
                                                    background: theme.palette.primary,
                                                    color: theme.palette.getContrastText(blue[500])
                                                }}
                                            >
                                                <Typography className="font-medium truncate" color="inherit">{g.Konu}</Typography>
                                                <div className="gorusmeHeader flex items-center justify-center opacity-75">
                                                    <Icon className="text-20 mx-8" color="inherit">access_time</Icon>
                                                    <div className="text-16 whitespace-no-wrap">{g.Zaman}</div>
                                                </div>
                                            </div>
                                            <CardContent className="flex flex-col flex-auto  justify-center">
                                                <Typography className="text-13 font-600 mt-4" color="textSecondary">{g.Aciklama}</Typography>
                                            </CardContent>
                                            <Divider />
                                            <CardActions className="flex flex-row justify-between">
                                                <Tooltip arrow
                                                    title={
                                                        <React.Fragment>
                                                            <Typography color="inherit">name</Typography>
                                                        </React.Fragment>
                                                    }
                                                >
                                                    <Avatar src="" className={classes.small} >
                                                    </Avatar>
                                                </Tooltip>
                                                <Tooltip arrow
                                                    title={
                                                        <React.Fragment>
                                                            <Typography color="inherit">name</Typography>
                                                        </React.Fragment>
                                                    }
                                                >
                                                    <Avatar src="" className={classes.small} >
                                                    </Avatar>
                                                </Tooltip>
                                            </CardActions>
                                        </Card>
                                    </div>
                                </div>
                            </React.Fragment>
                        )
                    }
                    )}
                </div>
            </div>
            <GorusmeDlg show={showGorusmeDlg} firmaId={firma._id} />

        </div>
    )
}
export default withReducer('satisApp', reducer)(FirmaGorusmeler);
