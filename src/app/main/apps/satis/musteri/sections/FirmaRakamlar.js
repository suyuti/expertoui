import React from 'react'
import {  TextField, Icon, Typography } from '@material-ui/core';
//import { useDispatch, useSelector } from 'react-redux';
//import * as Actions from '../../store/actions';
//import { makeStyles, useTheme } from '@material-ui/styles';
//import { orange } from '@material-ui/core/colors';

function FirmaRakamlar(props) {
    var { firma, handleChange } = props

    console.log(firma.CalisanSayisi)


    return (
        <div className="p-16 sm:p-24 max-w-2xl">

            <div className="pb-48">
                <div className="pb-16 flex items-center">
                    <Icon color="action">access_time</Icon>
                    <Typography className="h2 mx-16" color="textSecondary">Rakamlar</Typography>
                </div>
                <div className="flex -mx-4">
                    <TextField
                        className="mt-8 mb-16 mx-4"
                        error={firma.CalisanSayisi === ''}
                        label="Çalışan Sayısı"
                        id="calisanSayisi"
                        name="CalisanSayisi"
                        value={firma.CalisanSayisi || 0}
                        onChange={handleChange}
                        variant="outlined"
                        fullWidth
                    />
                    <TextField
                        className="mt-8 mb-16 mx-4"
                        error={firma.TeknikPersonelSayisi === ''}
                        label="Teknik Personel Sayısı"
                        id="teknikPersonelSayisi"
                        name="TeknikPersonelSayisi"
                        value={firma.TeknikPersonelSayisi || 0}
                        onChange={handleChange}
                        variant="outlined"
                        fullWidth
                    />
                </div>
                <div className="flex -mx-4">
                    <TextField
                        className="mt-8 mb-16 mx-4"
                        error={firma.MuhendisSayisi === ''}
                        label="Mühendis Sayısı"
                        id="muhendisSayisi"
                        name="MuhendisSayisi"
                        value={firma.MuhendisSayisi || 0}
                        onChange={handleChange}
                        variant="outlined"
                        fullWidth
                    />
                    <TextField
                        className="mt-8 mb-16 mx-4"
                        error={firma.Ciro === ''}
                        label="Ciro"
                        id="ciro"
                        name="Ciro"
                        value={firma.Ciro || 0}
                        onChange={handleChange}
                        variant="outlined"
                        fullWidth
                    />
                </div>
            </div>
        </div>
    )
}
export default FirmaRakamlar