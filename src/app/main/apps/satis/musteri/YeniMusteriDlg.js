import React, {useState} from 'react'
import { TextField, Button, Dialog, DialogActions, DialogContent, Icon, IconButton, Typography, Toolbar, AppBar, Avatar } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import { useForm } from '@fuse/hooks';
import * as Actions from '../store/actions';


const defaultFormState = {
    id: '',
    name: '',
    lastName: '',
    avatar: 'assets/images/avatars/profile.jpg',
    nickname: '',
    company: '',
    jobTitle: '',
    email: '',
    phone: '',
    address: '',
    birthday: '',
    notes: ''
};

function YeniMusteriDlg(props) {
    const dispatch = useDispatch();

    const showMusteriDlg = useSelector(({ satisApp }) => satisApp.musteri.showYeniMusteriDlg);
    //const { form, handleChange, setForm } = useForm(defaultFormState);

    const [form, setform] = useState({})

    function handleChange(e) {
        var f = {...form}
        f[e.target.name] = e.target.value
        setform(f)
    }

    function handleSubmit(e) {
        e.preventDefault()
        dispatch(Actions.saveMusteri(form))
        dispatch(Actions.closeYeniMusteriDlg())
        //props.history.goBack()
    }

    return (
        <Dialog
            classes={{
                paper: "m-24"
            }}
            open={showMusteriDlg}
            fullWidth
            maxWidth="xs"
        >
            <AppBar position="static" elevation={1}>
                <Toolbar className="flex w-full">
                    <Typography variant="subtitle1" color="inherit">
                        Yeni Müşteri
                    </Typography>
                </Toolbar>
                <div className="flex flex-col items-center justify-center pb-24">
                </div>
            </AppBar>
            <form noValidate
                //onSubmit={handleSubmit} 
                className="flex flex-col md:overflow-hidden">
                <DialogContent classes={{ root: "p-24" }}>
                    <div className="flex">
                        <div className="min-w-48 pt-20">
                            <Icon color="action">account_circle</Icon>
                        </div>

                        <TextField
                            className="mb-24"
                            label="Firma Ünvanı"
                            autoFocus
                            id="Unvani"
                            name="Unvani"
                            value={form.Unvani}
                            onChange={handleChange}
                            variant="outlined"
                            required
                            fullWidth
                        />
                    </div>
                    <div className="flex">
                        <div className="min-w-48 pt-20">
                            <Icon color="action">account_circle</Icon>
                        </div>
                        <TextField
                            className="mb-24"
                            label="Firma Markası"
                            id="Markasi"
                            name="Markasi"
                            value={form.Markasi}
                            onChange={handleChange}
                            variant="outlined"
                            required
                            fullWidth
                        />
                    </div>

                </DialogContent>

            <DialogActions className="justify-between p-8">
            <div className="px-16">
                    <Button
                        variant="contained"
                        color="primary"
                        onClick={handleSubmit}
                        type="submit"
                    //disabled={!canBeSubmitted()}
                    >
                        Ekle
                            </Button>
                </div>
                <div className="px-16">
                    <Button
                        variant="contained"
                        color="primary"
                        onClick={() => {dispatch(Actions.closeYeniMusteriDlg())}}
                        //type="submit"
                    //disabled={!canBeSubmitted()}
                    >
                        Kapat
                            </Button>
                </div>
            </DialogActions>
            </form>

        </Dialog >
    )
}

export default YeniMusteriDlg