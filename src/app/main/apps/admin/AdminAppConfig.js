import React from 'react';
import {Redirect} from 'react-router-dom';
import { authRoles } from 'app/auth';

export const AdminAppConfig = {
    settings: {
        layout: {
            config: {}
        }
    },
    //auth: authRoles.admin,
    routes  : [
        {
            path     : '/apps/admin/users/:id',
            component: React.lazy(() => import('./AdminApp'))
        },
        {
            path     : '/apps/admin/users',
            component: React.lazy(() => import('./AdminApp'))
        },
        {
            path     : '/apps/admin',
            component: React.lazy(() => import('./dashboard/AdminDashboard'))
        },
    ]
};
