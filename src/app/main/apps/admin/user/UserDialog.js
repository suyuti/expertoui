import React, {useEffect, useCallback} from 'react';
import {TextField, Button, Dialog, DialogActions, DialogContent, Icon, IconButton, Typography, Toolbar, AppBar, Avatar, MenuItem} from '@material-ui/core';
import {useForm} from '@fuse/hooks';
import FuseUtils from '@fuse/FuseUtils';
import * as Actions from '../store/actions';
import {useDispatch, useSelector} from 'react-redux';

const defaultFormState = {
    id      : '',
    name    : '',
    lastName: '',
    avatar  : 'assets/images/avatars/profile.jpg',
    nickname: '',
    company : '',
    jobTitle: '',
    email   : '',
    phone   : '',
    address : '',
    birthday: '',
    notes   : ''
};

function UserDialog(props)
{
    const dispatch      = useDispatch();
    const userDialog    = useSelector(({adminApp}) => adminApp.users.userDialog);

    const {form, handleChange, setForm} = useForm(defaultFormState);

    const roller = [
        {key: "yo", adi: "Yönetici" },
        {key: "sy", adi: "Satış Yöneticici" },
        {key: "sp", adi: "Satış Personeli" },
        {key: "ty", adi: "Teknik Yönetici" },
        {key: "tp", adi: "Teknik Personeli" },
        {key: "my", adi: "Mali Yönetici" },
        {key: "mp", adi: "Mali Personeli" },
        {key: "mu", adi: "Müşteri" },
    ]
    const initDialog = useCallback(
        () => {
            /**
             * Dialog type: 'edit'
             */
            if ( userDialog.type === 'edit' && userDialog.data )
            {
                setForm({...userDialog.data});
            }

            /**
             * Dialog type: 'new'
             */
            if ( userDialog.type === 'new' )
            {
                setForm({
                    ...defaultFormState,
                    ...userDialog.data,
                    id: FuseUtils.generateGUID()
                });
            }
        },
        [userDialog.data, userDialog.type, setForm],
    );

    useEffect(() => {
        /**
         * After Dialog Open
         */
        if ( userDialog.props.open )
        {
            initDialog();
        }

    }, [userDialog.props.open, initDialog]);

    function closeComposeDialog()
    {
        userDialog.type === 'edit' ? dispatch(Actions.closeEditUserDialog()) : dispatch(Actions.closeNewUserDialog());
    }

    function canBeSubmitted()
    {
        return (
            true
            //form.name.length > 0
        );
    }

    function handleSubmit(event)
    {
        event.preventDefault();

        if ( userDialog.type === 'new' )
        {
            dispatch(Actions.addUser(form));
        }
        else
        {
            dispatch(Actions.updateUser(form));
        }
        closeComposeDialog();
    }

    function handleRemove()
    {
        dispatch(Actions.removeUser(form._id));
        closeComposeDialog();
    }

    return (
        <Dialog
            classes={{
                paper: "m-24"
            }}
            {...userDialog.props}
            onClose={closeComposeDialog}
            fullWidth
            maxWidth="xs"
        >

            <AppBar position="static" elevation={1}>
                <Toolbar className="flex w-full">
                    <Typography variant="subtitle1" color="inherit">
                        {userDialog.type === 'new' ? 'Yeni Kullanıcı' : 'Kullanıcı Güncelleme'}
                    </Typography>
                </Toolbar>
                <div className="flex flex-col items-center justify-center pb-24">
                    <Avatar className="w-96 h-96" alt="contact avatar" src={form.avatar}/>
                    {userDialog.type === 'edit' && (
                        <Typography variant="h6" color="inherit" className="pt-8">
                            {form.name}
                        </Typography>
                    )}
                </div>
            </AppBar>
            <form noValidate onSubmit={handleSubmit} className="flex flex-col md:overflow-hidden">
                <DialogContent classes={{root: "p-24"}}>
                    <div className="flex">
                        <div className="min-w-48 pt-20">
                            <Icon color="action">account_circle</Icon>
                        </div>

                        <TextField
                            className="mb-24"
                            label="Adı"
                            autoFocus
                            id="FirstName"
                            name="FirstName"
                            value={form.FirstName}
                            onChange={handleChange}
                            variant="outlined"
                            required
                            fullWidth
                        />
                    </div>

                    <div className="flex">
                        <div className="min-w-48 pt-20">
                        </div>
                        <TextField
                            className="mb-24"
                            label="Soyadı"
                            id="LastName"
                            name="LastName"
                            value={form.LastName}
                            onChange={handleChange}
                            variant="outlined"
                            fullWidth
                        />
                    </div>

                    <div className="flex">
                        <div className="min-w-48 pt-20">
                            <Icon color="action">star</Icon>
                        </div>
                        <TextField
                            className="mb-24"
                            label="Kullanıcı adı"
                            id="UserName"
                            name="UserName"
                            value={form.UserName}
                            onChange={handleChange}
                            variant="outlined"
                            fullWidth
                        />
                    </div>

                    <div className="flex">
                        <div className="min-w-48 pt-20">
                            <Icon color="action">star</Icon>
                        </div>
                        <TextField
                            className="mb-24"
                            label="Rol"
                            id="Role"
                            name="Role"
                            value={form.Role}
                            onChange={handleChange}
                            variant="outlined"
                            fullWidth
                            select
                        >
                            {roller.map(r => (
                                <MenuItem key={r.key} value={r.key}>{r.adi}</MenuItem>
                            ))}
                        </TextField>
                    </div>

                    <div className="flex">
                        <div className="min-w-48 pt-20">
                            <Icon color="action">phone</Icon>
                        </div>
                        <TextField
                            className="mb-24"
                            label="Phone"
                            id="phone"
                            name="phone"
                            value={form.phone}
                            onChange={handleChange}
                            variant="outlined"
                            fullWidth
                        />
                    </div>

                    <div className="flex">
                        <div className="min-w-48 pt-20">
                            <Icon color="action">email</Icon>
                        </div>
                        <TextField
                            className="mb-24"
                            label="Email"
                            id="email"
                            name="email"
                            value={form.email}
                            onChange={handleChange}
                            variant="outlined"
                            fullWidth
                        />
                    </div>

                    <div className="flex">
                        <div className="min-w-48 pt-20">
                            <Icon color="action">domain</Icon>
                        </div>
                        <TextField
                            className="mb-24"
                            label="Company"
                            id="company"
                            name="company"
                            value={form.company}
                            onChange={handleChange}
                            variant="outlined"
                            fullWidth
                        />
                    </div>

                    <div className="flex">
                        <div className="min-w-48 pt-20">
                            <Icon color="action">work</Icon>
                        </div>
                        <TextField
                            className="mb-24"
                            label="Job title"
                            id="jobTitle"
                            name="jobTitle"
                            value={form.jobTitle}
                            onChange={handleChange}
                            variant="outlined"
                            fullWidth
                        />
                    </div>

                    <div className="flex">
                        <div className="min-w-48 pt-20">
                            <Icon color="action">cake</Icon>
                        </div>
                        <TextField
                            className="mb-24"
                            id="birthday"
                            label="Birthday"
                            type="date"
                            value={form.birthday}
                            onChange={handleChange}
                            InputLabelProps={{
                                shrink: true
                            }}
                            variant="outlined"
                            fullWidth
                        />
                    </div>

                    <div className="flex">
                        <div className="min-w-48 pt-20">
                            <Icon color="action">home</Icon>
                        </div>
                        <TextField
                            className="mb-24"
                            label="Address"
                            id="address"
                            name="address"
                            value={form.address}
                            onChange={handleChange}
                            variant="outlined"
                            fullWidth
                        />
                    </div>

                    <div className="flex">
                        <div className="min-w-48 pt-20">
                            <Icon color="action">note</Icon>
                        </div>
                        <TextField
                            className="mb-24"
                            label="Notes"
                            id="notes"
                            name="notes"
                            value={form.notes}
                            onChange={handleChange}
                            variant="outlined"
                            multiline
                            rows={5}
                            fullWidth
                        />
                    </div>
                </DialogContent>

                {userDialog.type === 'new' ? (
                    <DialogActions className="justify-between p-8">
                        <div className="px-16">
                            <Button
                                variant="contained"
                                color="primary"
                                onClick={handleSubmit}
                                type="submit"
                                disabled={!canBeSubmitted()}
                            >
                                Add
                            </Button>
                        </div>
                    </DialogActions>
                ) : (
                    <DialogActions className="justify-between p-8">
                        <div className="px-16">
                            <Button
                                variant="contained"
                                color="primary"
                                type="submit"
                                onClick={handleSubmit}
                                disabled={!canBeSubmitted()}
                            >
                                Save
                            </Button>
                        </div>
                        <IconButton
                            onClick={handleRemove}
                        >
                            <Icon>delete</Icon>
                        </IconButton>
                    </DialogActions>
                )}
            </form>
        </Dialog>
    );
}

export default UserDialog;
