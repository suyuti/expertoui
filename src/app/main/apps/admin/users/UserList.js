import React, { useEffect, useState } from 'react';
import { Avatar, Checkbox, Icon, IconButton, Typography } from '@material-ui/core';
import { FuseUtils, FuseAnimate } from '@fuse';
import { useDispatch, useSelector } from 'react-redux';
import ReactTable from "react-table";
import * as Actions from '../store/actions';
//import UsersMultiSelectMenu from './UsersMultiSelectMenu';
import Switch from '@material-ui/core/Switch';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';

function UserList(props) {
    const dispatch = useDispatch();

    const users = useSelector(({ adminApp }) => adminApp.users.entities);
    const selectedUserIds = useSelector(({ adminApp }) => adminApp.users.selectedUserIds);
    const searchText = useSelector(({ adminApp }) => adminApp.users.searchText);
    const user = useSelector(({ adminApp }) => adminApp.user);

    const [filteredData, setFilteredData] = useState(null);

    useEffect(() => {
        function getFilteredArray(entities, searchText) {
            const arr = Object.keys(entities).map((id) => entities[id]);
            if (searchText.length === 0) {
                return arr;
            }
            return FuseUtils.filterArrayByString(arr, searchText);
        }

        if (users) {
            setFilteredData(getFilteredArray(users, searchText));
        }
    }, [users, searchText]);


    if (!filteredData) {
        return null;
    }

    if (filteredData.length === 0) {
        return (
            <div className="flex flex-1 items-center justify-center h-full">
                <Typography color="textSecondary" variant="h5">
                    Kullanici yok!
                </Typography>
            </div>
        );
    }

    return (
        <FuseAnimate animation="transition.slideUpIn" delay={300}>
            <ReactTable
                className="-striped -highlight h-full sm:rounded-16 overflow-hidden"
                getTrProps={(state, rowInfo, column) => {
                    return {
                        className: "cursor-pointer",
                        onClick: (e, handleOriginal) => {
                            if (rowInfo) {
                                dispatch(Actions.openEditUserDialog(rowInfo.original));
                            }
                        }
                    }
                }}
                data={filteredData}
                columns={[
                    {
                        Header: () => (
                            <Checkbox
                                onClick={(event) => {
                                    event.stopPropagation();
                                }}
                                onChange={(event) => {
                                    event.target.checked ? dispatch(Actions.selectAllUsers()) : dispatch(Actions.deSelectAllUsers());
                                }}
                                checked={selectedUserIds.length === Object.keys(users).length && selectedUserIds.length > 0}
                                indeterminate={selectedUserIds.length !== Object.keys(users).length && selectedUserIds.length > 0}
                            />
                        ),
                        accessor: "",
                        Cell: row => {
                            return (<Checkbox
                                onClick={(event) => {
                                    event.stopPropagation();
                                }}
                                checked={selectedUserIds.includes(row.value._id)}
                                onChange={() => dispatch(Actions.toggleInSelectedUsers(row.value._id))}
                            />
                            )
                        },
                        className: "justify-center",
                        sortable: false,
                        width: 64
                    },
/*                    {
                        Header   : () => (
                            selectedUserIds.length > 0 && (
                                <UsersMultiSelectMenu/>
                            )
                        ),
                        accessor : "avatar",
                        Cell     : row => (
                            <Avatar className="mx-8" alt={row.original.name} src={row.value}/>
                        ),
                        className: "justify-center",
                        width    : 64,
                        sortable : false
                    },
  */                  {
                        Header: "Adı",
                        accessor: "FirstName",
                        filterable: true,
                        className: "font-bold"
                    },
                    {
                        Header: "Soyadı",
                        accessor: "lastName",
                        filterable: true,
                        className: "font-bold"
                    },
                    {
                        Header: "Kullanici adi",
                        accessor: "UserName",
                        filterable: true
                    },
                    {
                        Header: "Yetki",
                        accessor: "Role",
                        filterable: true
                    },
                    {
                        Header: "Email",
                        accessor: "email",
                        filterable: true
                    },
                    {
                        Header: "Phone",
                        accessor: "phone",
                        filterable: true
                    },
                    {
                        Header: "",
                        width: 128,
                        Cell: row => (
                            <div className="flex items-center">
                            <div className="flex flex-col items-center">
                                <Typography>Aktif</Typography>
                                <Switch
                                    checked={row.original.Active}
                                    color="primary"
                                    onClick={(ev) => {
                                        ev.stopPropagation();
                                        dispatch(Actions.toggleActiveUser(row.original._id))
                                        //alert('change')
                                    }}
                                ></Switch>
                            </div>
                                <IconButton
                                    onClick={(ev) => {
                                        ev.stopPropagation();
                                        dispatch(Actions.removeUser(row.original._id))
                                        //dispatch(Actions.removeUser(row.original._id));
                                    }}
                                >
                                    <Icon>delete</Icon>
                                </IconButton>
                            </div>
                        )
                    }
                ]}
                defaultPageSize={10}
                noDataText="No Users found"
            />
        </FuseAnimate>
    );
}

export default UserList;
