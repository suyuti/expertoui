import React, {useRef, useEffect} from 'react'
import { FusePageSimple, FuseLoading } from '@fuse'
import {useDispatch, useSelector} from 'react-redux';
import * as Actions from '../store/actions'
import reducer from '../store/reducers'
import  Widget1 from '../../../components/Widgets/Widget1'
import withReducer from 'app/store/withReducer';

function AdminDashboard(props) {
    const dispatch = useDispatch();
    const pageLayout = useRef(null);
    const users = useSelector(({adminApp}) => adminApp.users.entities);

    useEffect(() => {
        function getUsers() {
            dispatch(Actions.getUsers({}))
        }
        getUsers()
    }, [])

    if (!users) {
        return <FuseLoading />
    }

    return (
        <React.Fragment>
            <FusePageSimple
                content = {
                    <div className="p-12">
                        <div className="widget flex w-full sm:w-1/2 md:w-1/6 p-12">
                            <Widget1 
                                title='Kullanıcılar' 
                                data={Object.keys(users).length}
                                link= "/apps/admin/users"/>
                        </div>
                    </div>
                }
                ref={pageLayout}
            />
        </React.Fragment>
    )
}

export default withReducer('adminApp', reducer)(AdminDashboard);
