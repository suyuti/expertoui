import axios from 'axios';
import {getUserData} from 'app/main/apps/admin/store/actions/user.actions';

export const GET_USERS                  = '[ADMIN APP] GET USERS';
export const SET_SEARCH_TEXT            = '[ADMIN APP] SET SEARCH TEXT';
export const TOGGLE_IN_SELECTED_USERS   = '[ADMIN APP] TOGGLE IN SELECTED USERS';
export const SELECT_ALL_USERS           = '[ADMIN APP] SELECT ALL USERS';
export const DESELECT_ALL_USERS         = '[ADMIN APP] DESELECT ALL USERS';
export const OPEN_NEW_USER_DIALOG       = '[ADMIN APP] OPEN NEW USER DIALOG';
export const CLOSE_NEW_USER_DIALOG      = '[ADMIN APP] CLOSE NEW USER DIALOG';
export const OPEN_EDIT_USER_DIALOG      = '[ADMIN APP] OPEN EDIT USER DIALOG';
export const CLOSE_EDIT_USER_DIALOG     = '[ADMIN APP] CLOSE EDIT USER DIALOG';
export const ADD_USER                   = '[ADMIN APP] ADD USER';
export const UPDATE_USER                = '[ADMIN APP] UPDATE USER';
export const REMOVE_USER                = '[ADMIN APP] REMOVE USER';
export const REMOVE_USERS               = '[ADMIN APP] REMOVE USERS';
export const TOGGLE_STARRED_USER        = '[ADMIN APP] TOGGLE STARRED USER';
export const TOGGLE_STARRED_USERS       = '[ADMIN APP] TOGGLE STARRED USERS';
export const SET_USERS_STARRED          = '[ADMIN APP] SET USERS STARRED ';
export const TOGGLE_ACTIVE_USER         = '[ADMIN APP] TOGGLE ACTIVE USER';

export function getUsers(routeParams)
{
    const request = axios.get('/api/user', {
        params: routeParams
    });

    return (dispatch) =>
        request.then((response) =>
            dispatch({
                type   : GET_USERS,
                payload: response.data,
                routeParams
            })
        );
}

export function setSearchText(event)
{
    return {
        type      : SET_SEARCH_TEXT,
        searchText: event.target.value
    }
}

export function toggleInSelectedUsers(userId)
{
    return {
        type: TOGGLE_IN_SELECTED_USERS,
        userId
    }
}

export function toggleActiveUser(userId)
{
    return (dispatch, getState) => {
        //const {routeParams} = getState().contactsApp.contacts;

        const request = axios.post(`/api/user/${userId}/activate`);

        return request.then((response) =>
            Promise.all([
                dispatch({
                    type: TOGGLE_ACTIVE_USER
                }),
                dispatch(getUsers())
                //dispatch(getUserData())
            ]).then(() => {
                //dispatch(getUsers(routeParams))
            })
        );
    };
}

export function selectAllUsers()
{
    return {
        type: SELECT_ALL_USERS
    }
}

export function deSelectAllUsers()
{
    return {
        type: DESELECT_ALL_USERS
    }
}

export function openNewUserDialog()
{
    return {
        type: OPEN_NEW_USER_DIALOG
    }
}

export function closeNewUserDialog()
{
    return {
        type: CLOSE_NEW_USER_DIALOG
    }
}

export function openEditUserDialog(data)
{
    return {
        type: OPEN_EDIT_USER_DIALOG,
        data
    }
}

export function closeEditUserDialog()
{
    return {
        type: CLOSE_EDIT_USER_DIALOG
    }
}

export function addUser(newUser)
{
    return (dispatch, getState) => {

        const {routeParams} = getState().contactsApp.contacts;

        const request = axios.post('/api/user/add', {
            newUser
        });

        return request.then((response) =>
            Promise.all([
                dispatch({
                    type: ADD_USER
                })
            ]).then(() => dispatch(getUsers(routeParams)))
        );
    };
}

export function updateUser(contact)
{
    return (dispatch, getState) => {

        //const {routeParams} = getState().contactsApp.contacts;

        const request = axios.put(`/api/user/${contact._id}`, {
            contact
        });

        return request.then((response) =>
            Promise.all([
                dispatch({
                    type: UPDATE_USER
                })
            ]).then(() => dispatch(getUsers({})))
        );
    };
}

export function removeUser(userId)
{
    return (dispatch, getState) => {

        const {routeParams} = getState().adminApp.users;

        const request = axios.delete(`/api/user/${userId}`);

        return request.then((response) =>
            Promise.all([
                dispatch({
                    type: REMOVE_USER
                })
            ]).then(() => dispatch(getUsers(routeParams)))
        );
    };
}


export function removeUsers(contactIds)
{
    return (dispatch, getState) => {

        const {routeParams} = getState().contactsApp.contacts;

        const request = axios.post('/api/user/remove-users', {
            contactIds
        });

        return request.then((response) =>
            Promise.all([
                dispatch({
                    type: REMOVE_USERS
                }),
                dispatch({
                    type: DESELECT_ALL_USERS
                })
            ]).then(() => dispatch(getUsers(routeParams)))
        );
    };
}

export function toggleStarredUser(contactId)
{
    return (dispatch, getState) => {
        const {routeParams} = getState().contactsApp.contacts;

        const request = axios.post('/api/user/toggle-starred-user', {
            contactId
        });

        return request.then((response) =>
            Promise.all([
                dispatch({
                    type: TOGGLE_STARRED_USER
                }),
                dispatch(getUserData())
            ]).then(() => dispatch(getUsers(routeParams)))
        );
    };
}

export function toggleStarredUsers(contactIds)
{
    return (dispatch, getState) => {

        const {routeParams} = getState().contactsApp.contacts;

        const request = axios.post('/api/user/toggle-starred-users', {
            contactIds
        });

        return request.then((response) =>
            Promise.all([
                dispatch({
                    type: TOGGLE_STARRED_USERS
                }),
                dispatch({
                    type: DESELECT_ALL_USERS
                }),
                dispatch(getUserData())
            ]).then(() => dispatch(getUsers(routeParams)))
        );
    };
}

export function setUsersStarred(contactIds)
{
    return (dispatch, getState) => {

        const {routeParams} = getState().contactsApp.contacts;

        const request = axios.post('/api/user/set-users-starred', {
            contactIds
        });

        return request.then((response) =>
            Promise.all([
                dispatch({
                    type: SET_USERS_STARRED
                }),
                dispatch({
                    type: DESELECT_ALL_USERS
                }),
                dispatch(getUserData())
            ]).then(() => dispatch(getUsers(routeParams)))
        );
    };
}

export function setUsersUnstarred(contactIds)
{
    return (dispatch, getState) => {

        const {routeParams} = getState().contactsApp.contacts;

        const request = axios.post('/api/user/set-users-unstarred', {
            contactIds
        });

        return request.then((response) =>
            Promise.all([
                dispatch({
                    type: SET_USERS_STARRED
                }),
                dispatch({
                    type: DESELECT_ALL_USERS
                }),
                dispatch(getUserData())
            ]).then(() => dispatch(getUsers(routeParams)))
        );
    };
}
