import React, { useEffect, useState } from 'react'
import { makeStyles } from '@material-ui/styles';
import { Icon, Typography, TextField, Button, Box, ButtonBase } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
    resize: {
        fontSize: 26
    },
    box: {
        margin: 0,
        padding: 0
    }
}));

function TextEdit(props) {
    const [edit, setEdit] = useState(false)
    const classes = useStyles(props);

    return (
        <React.Fragment>
            <div style={{ width: '100%' }}>
                <Box className={classes.box} display="flex" p={1} >
                    <Box className={classes.box} p={1} alignItems="center" flexGrow={1} >
                        {!edit ?
                            (
                                <Box className={classes.box} p={1}>
                                    <Typography flexGrow={1} p={1} alignItems="center" className={classes.resize} gutterBottom>{props.value}</Typography>
                                </Box>
                            )
                            :
                            (
                                <Box className={classes.box} p={1}>
                                    <TextField
                                        autoFocus
                                        alignItems="center" 
                                        flexGrow={1} 
                                        p={1}
                                        {...props}
                                        InputProps={{
                                            classes: {
                                                input: classes.resize,
                                            },
                                        }}
                                    />
                                </Box>
                            )}
                    </Box>
                    <Box className={classes.box} p={1} display="flex" alignItems="center">
                        <ButtonBase>
                            <Icon 
                                fontSize="large"
                                color="action" 
                                onClick={() => {
                            setEdit(!edit)}}>{!edit?"edit":"save_alt"}</Icon>
                        </ButtonBase>
                    </Box>
                </Box>
            </div>


            {/*
            <div style={{width:'100%'}}>
                <Box display='flex' p={1} bgcolor="grey.300">

            {edit ? 
                (
                    <Box p={1}>
                        <Typography flexGrow={1} p={1}  className={classes.resize} gutterBottom>{props.value}</Typography>
                    </Box>
                ) 
                : 
                (
                    <Box p={1}>
                    <TextField flexGrow={1} p={1} 
                        {...props}
                        InputProps={{
                            classes: {
                                input: classes.resize,
                            },
                        }}
                    />
                    </Box>
                )}
                <Box p={1}>
                    <Button   onClick={() => setEdit(!edit)}>Edit</Button>
                </Box>
                </Box>
            </div>
                    */}
        </React.Fragment>
    )
}
export default TextEdit