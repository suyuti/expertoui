import React from 'react'
import TimelineItem from './TimelineItem'
import './timeline.css'

export default function Timeline(props) {
    return (
        <div className="timeline-container">
            {props.children}
        </div>
    )
}