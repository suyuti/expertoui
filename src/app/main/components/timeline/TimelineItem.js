import React from 'react'
import './timeline.css'
import { Chip, Avatar, Icon, Typography } from '@material-ui/core'

export default function TimelineItem({ data }) {
    return (
        <div className="timeline-item">
            <div class="box">
                <i class="fas fa-camera box-icon"></i>
                <div class="header">
                    <h1>Header</h1>
                    <i class="far fa-clock"><span>00:21</span></i>
                </div>
                <div class="body">
                    <p>eferverv re vrt vkrtv ltrkv otr votvtilvlit rt vklrlgv igrv lrtjvlrijt vlritj vrjt lkkrgn v;krjlrtj v;rt ijrtlijfdflj dflhkfh kgu ltrh gjtr i;ltg kfg</p>
                </div>
                <div class="footer">
                    <div class="kisiler">
                        <div class="kisi">
                            <i class="fas fa-camera avatar"></i>
                            <p>footer</p>
                        </div>
                        <div class="kisi">
                            <i class="fas fa-camera avatar"></i>
                            <p>footer</p>
                        </div>
                    </div>
                    <div class="temsilci">
                        <div class="kisi">
                            <i class="fas fa-camera avatar"></i>
                            <p>Ahmet</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}