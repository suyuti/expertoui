import React from 'react';
import { Paper, Typography, IconButton, Icon } from '@material-ui/core';
import { Link } from 'react-router-dom';

function Widget1(props) {
    return (
        <Paper className="w-full rounded-8 shadow-none border-1">
            <div className="flex items-center justify-between px-4 pt-4">
                <Typography className="text-16 px-12">{props.title}</Typography>
                <IconButton area-label="more">
                    <Icon>more_vert</Icon>
                </IconButton>
            </div>
            <div className="text-center pt-12 pb-28">
                <Typography className="text-72 leading-none text-red">{props.data || ''}</Typography>
            </div>
            <div className="flex items-center px-16 h-52 border-t-1">
                <Typography className="text-15 flex w-full " color="textSecondary" component={Link} to={props.link}>Detay</Typography>
            </div>
        </Paper>
    )
}

export default React.memo(Widget1);