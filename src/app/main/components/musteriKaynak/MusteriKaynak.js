import React from 'react'
import {TextField, Typography} from '@material-ui/core';

function MusteriKaynak(props) {
    const {tipi} = props

    return (
        <React.Fragment>
            {tipi === "Reklam" && 
                <>
                <TextField id="ReklamTarihi"></TextField>
                <TextField id="ReklamOrtami"></TextField>
                <TextField id="ReklamAciklama"></TextField>
                </>
            }
            {tipi === "Mail" && 
                <>
                <TextField id="MailTarihi"></TextField>
                <TextField id="MailBaslik"></TextField>
                <TextField id="MailAciklama"></TextField>
                </>
            }
            {tipi === "İç Personel Yönlendirme" && 
                <>
                <TextField id="IcPersonelYonlendirmeKim"></TextField>
                <TextField id="IcPersonelYonlendirmeAciklama"></TextField>
                </>
            }
            {tipi === "Partner" && 
                <>
                <TextField id="Partner"></TextField>
                <TextField id="PartnerOran"></TextField>
                <TextField id="PartnerTarih"></TextField>
                <TextField id="PartnerAciklama"></TextField>
                </>
            }
            {tipi === "Referans" && 
                <>
                <TextField id="ReferansMusteri"></TextField>
                <TextField id="ReferansTarih"></TextField>
                <TextField id="ReferansAciklama"></TextField>
                </>
            }
            {tipi === "Web" && 
                <>
                <TextField id="WebTarih"></TextField>
                <TextField id="WebAciklama"></TextField>
                </>
            }
            {tipi === "Fuar" && 
                <>
                <TextField id="HangiFuar"></TextField>
                <TextField id="FuarTarihi"></TextField>
                <TextField id="FuarAciklama"></TextField>
                </>
            }

        </React.Fragment>
    )
}

export default MusteriKaynak