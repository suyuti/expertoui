import axios from 'axios'

let instance = axios.create({
    baseURL : process.env.REACT_APP_BASE_URL
})

async function refreshToken(config) { 
    config.headers.Authorization=localStorage.getItem('jwt_access_token') 
    return config
}

instance.interceptors.request.use((config) => {
    return refreshToken(config);
}, error => {
    return Promise.reject(error)
})

instance.interceptors.response.use((response) => {
    return parseBody(response)
}, error => {
    if (error.response) {
        return parseError(error.response.data)
    } else {
        return Promise.reject(error)
    }
})

function parseError(messages) {
    if (messages) {
        if (messages instanceof Array) {
            return Promise.reject({ messages: messages })
        } else {
            return Promise.reject({ messages: [messages] })
        }
    } else {
        return Promise.reject({ messages: ['Bir hata oluştu'] })
    }
}

function parseBody(response) {
    if (response.status != 200) {
        console.error(response.data)
    }
    if (response.status === 200) {

        return response.data
    } else {
        return parseError(response.data.messages)
    }
}

export const http = instance