const locale = {
    'APPLICATIONS': 'Programlar',
    'EXAMPLE'     : 'Örnek Sayfa',
    'MUSTERILER'    : 'Müşteriler',
    'POTANSIYEL_MUSTERILER'    : 'Potansiyel Müşteriler',
    'AKTIF_MUSTERILER'    : 'Aktif Müşteriler',
    'PASIF_MUSTERILER'    : 'Pasif Müşteriler',
    'SORUNLU_MUSTERILER'    : 'Sorunlu Müşteriler',
    'URUNLER' : "Ürünler",  
    'SATIS_YONETIM_DASHBOARD': "Satış Yönetim Masası",
    'SATIS_DASHBOARD': "Satış Masası",
    "KISILER": "Kişiler"
};

export default locale;
