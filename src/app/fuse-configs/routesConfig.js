import React from 'react';
import {Redirect} from 'react-router-dom';
import {FuseUtils} from '@fuse';
import {ExampleConfig} from 'app/main/example/ExampleConfig';
import {LoginConfig} from 'app/main/login/LoginConfig';
import {RegisterConfig} from 'app/main/register/RegisterConfig';
import {AdminAppConfig} from 'app/main/apps/admin/AdminAppConfig';
import {SatisAppConfig} from 'app/main/apps/satis/SatisAppConfig';
import {UrunAppConfig} from 'app/main/apps/urun/UrunAppConfig';
import {SatisBoardAppConfig} from 'app/main/apps/satisfirsat/SatisBoardAppConfig';
import {TodoAppConfig} from 'app/main/apps/task/TodoAppConfig';
import Login from 'app/main/login/Login';

const routeConfigs = [
    AdminAppConfig,
    //ExampleConfig,
    LoginConfig,
    RegisterConfig,
    
    SatisAppConfig,
    //UrunAppConfig,
    //SatisBoardAppConfig,
    //TodoAppConfig
];

const routes = [
    ...FuseUtils.generateRoutesFromConfigs(routeConfigs, ['sa', 'ad', 'yo', 'sy', 'ty', 'my', 'sp', 'tp', 'mp', 'mu', 'gu']),
    {
        path     : '/',
        exact: true,
        auth: [],
        component:  () =>  {return (<Redirect to="/login"/>)}
    }
    /*    {
        path     : '/',
        exact: true,
        component: () => <Redirect to="/login"/>
    },*/
    ,{
        component: () => <Redirect to="/pages/errors/error-404"/>
    }
    
];

export default routes;
