import i18next from 'i18next';
import en from './navigation-i18n/en';
import tr from './navigation-i18n/tr';
import ar from './navigation-i18n/ar';

i18next.addResourceBundle('en', 'navigation', en);
i18next.addResourceBundle('tr', 'navigation', tr);
i18next.addResourceBundle('ar', 'navigation', ar);

const navigationConfig = [
    {
        'id'       : 'satis-yonetim-dashboard',
        'title'    : 'Satış Yönetim',
        'translate': 'SATIS_YONETIM_DASHBOARD',
        'type'     : 'item',
        'icon'     : 'whatshot',
        'auth'      : ['sa', 'ad', 'yo', 'sy'],
        'url'      : '/apps/satis/yonetim'
    },
    {
        'id'       : 'satis-dashboard',
        'title'    : 'Satış Masası',
        'translate': 'SATIS_DASHBOARD',
        'type'     : 'item',
        'icon'     : 'whatshot',
        'auth'      : ['sp'],
        'url'      : '/apps/satis'
    },
    {
        'id'       : 'satis',
        'title'    : 'Satis',
        'translate': 'SATIS',
        'type'     : 'group',
        'icon'     : 'apps',
        'auth'      : ['sa', 'ad', 'yo', 'sy', 'sp'],
        'children' : [
            {
                'id'       : 'musteriler',
                'title'    : 'Musteriler',
                'translate': 'MUSTERILER',
                'type'     : 'collapse',
                'icon'     : 'dashboard',
                'children' : [
                    {
                        'id'       : 'satis-musteri-potansiyel',
                        'title'    : 'Potansiyel Musteriler',
                        'translate': 'POTANSIYEL_MUSTERILER',
                        'type'     : 'item',
                        'icon'     : 'whatshot',
                        'url'      : '/apps/satis/musteriler/potansiyel'
                    },
                    {
                        'id'       : 'satis-musteri-aktif',
                        'title'    : 'Aktif Musteriler',
                        'translate': 'AKTIF_MUSTERILER',
                        'type'     : 'item',
                        'icon'     : 'whatshot',
                        'url'      : '/apps/satis/musteriler/aktif'
                    },
                    {
                        'id'       : 'satis-musteri-pasif',
                        'title'    : 'Pasif  Musteriler',
                        'translate': 'PASIF_MUSTERILER',
                        'type'     : 'item',
                        'icon'     : 'whatshot',
                        'url'      : '/apps/satis/musteriler/pasif'
                    },
                    {
                        'id'       : 'satis-musteri-sorunlu',
                        'title'    : 'Sorunlu Musteriler',
                        'translate': 'SORUNLU_MUSTERILER',
                        'type'     : 'item',
                        'icon'     : 'whatshot',
                        'url'      : '/apps/satis/musteriler/sorunlu'
                    }
                ]
            },
            {
                'id'        : 'kisiler',
                'title'     : 'Kisiler',
                'translate' : 'KISILER',
                'type'      : 'item',
                'icon'      : 'whatshot',
                'url'       : '/apps/satis/kisiler'
            }
        ]
    },
    {
        'id'       : 'urun',
        'title'    : 'Ürünler',
        'translate': 'URUNLER',
        'type'     : 'group',
        'icon'     : 'apps',
        'auth'      : ['sa', 'ad', 'yo', 'sy', 'sp'],
        'children'  : [
            {
                'id'       : 'urunler',
                'title'    : 'Ürünler',
                'translate': 'URUNLER',
                'type'     : 'collapse',
                'icon'     : 'dashboard',
                'children'  : [
                    {
                        'id'       : 'urun-1',
                        'title'    : 'Ürünler',
                        'translate': 'URUNLER',
                        'type'     : 'item',
                        'icon'     : 'whatshot',
                        'url'      : '/apps/urunler'
                    },
                ]
            }
        ]
    }
];

export default navigationConfig;
