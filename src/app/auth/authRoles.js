/**
 * Authorization Roles
 */
const authRoles = {
    superAdmin      : ['sa'],
    admin           : ['sa', 'ad'],
    yonetici        : ['sa', 'ad', 'yo'],
    satisYonetici   : ['sa', 'ad', 'yo', 'sy'],
    satisPersonel   : ['sa', 'ad', 'yo', 'sy', 'sp'],
    teknikYonetici  : ['sa', 'ad', 'yo', 'ty'],
    teknikPersonel  : ['sa', 'ad', 'yo', 'ty', 'tp'],
    maliYonetici    : ['sa', 'ad', 'yo', 'my'],
    maliPersonel    : ['sa', 'ad', 'yo', 'my', 'mp'],
    musteri         : ['sa', 'ad', 'yo', 'mu'],    
    onlyGuest       : []

    //admin    : ['admin'],
    //staff    : ['admin', 'staff'],
    //user     : ['admin', 'staff', 'user'],
};

export default authRoles;
