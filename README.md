    DB                  BE          UI
-----------------------------------------------    
ExpertoCRM_dev          dev         test
ExpertoCRM              prod        prod
ExpertoCRM_dev          local       local



scp -r ./* root@ :/var/www/html/experto_dev

nginx:
/var/www/html/experto_dev dizini altına kopyalanır

/etc/ngınx/sıtes-avaılable/defult
root /var/www/htm/experto_dev


ln -s /etc/nginx/sites-available/web1.webdock.io.conf /etc/nginx/sites-enabled/
ln -s /etc/nginx/sites-available/web2.webdock.io.conf /etc/nginx/sites-enabled/

sudo systemctl restart nginx


/// 
Tools:
backup_dev.sh:
    ExpertoAPI_dev veritabanini yedekler
backup_prod.sh:
    ExpertoAPI veritabanini yedekler
transfer_prod_to_dev.sh
    ExpertoAPI veritabanini ExpertoAPI_dev'e kopyalar. Test amaci ile prod verileri uzeriinde calimak icin kullanilir.


// topology

Database:
    ExpertoAPI: Prod veritabanidir
    ExpertoAPI_dev: test veritabanidir.

    ExpCrmAPI 206.189.186.106 makinasinda bulunuyor.
    admin baglanti icin:

    user: mongo-admin
    pwd: suyuti9419

    user: mongo-root
    pwd: suyuti9419

    prod ve test veritabanlari icin:
    user: experto
    pwd: suyuti9419


Backend:
    ExpCrmAPI 206.189.186.106 makinasinda bulunuyor.
    Hem test hem de Prod ayri ayri sunuluyor.
    Test: 4001 portundan dinliyor
    Prod: 4000 portundan dinliyor

    Her iki uygulama kendi dizinlerinde ayri kopyalari var.
    pm2 ile calistiriliyorlar.

    Test dizininde
    pm2 start npm --name "test" -- run start-test

    Prod dizininde
    pm2 start npm --name "prod" -- run start-prod

    Test uygulamasi ExpertoAPI_dev veritabanina sorgular
    Prod uygulamasi ExpertoAPI veritabanina sorgular

Frontend:
    ExpCrmUi 206.81.1.111 makinasinda calisir
    nginx sunar

    Prod surumu 80 portundan
    Test surumu 81 portundan sunulur

    Test surumu ExpCrmAPI 206.189.186.106 makinasinda 4001'e baglanir
    Prod surumu ExpCrmAPI 206.189.186.106 makinasinda 4000'e baglanir
    Dev surumu gelistirme ortaminda calisan backend'e baglanir. Localde 4001'den dinler. Locladeki backend ExpertoAPI_dev'e sorgular.

Derleme:
    Backend:
        yarn start-dev
            4001'den dinler. ExpertoAPI_dev veritabanini sorgular.
        yarn start-prod
            4000'den dinler. ExpertoAPI veritabanini sorgular.

    Forntend:
        yarn start:dev
            Localdeki backende baglanir. 4001, DB Test'e sorgular
        yarn start:test
            ExpCrmAPI makinasinda Test sunucusuna baglanir. 4001. DB Test'e sorgular
        yarn start:prod
            ExpCrmAPI makinasinda Prod sunucusuna baglanir. 4000. DB Prod'a sorgular
             
        yarn build:dev
        yarn build:test
        yarn build:prod
                
        yarn publish:test
            ExpCrmAPI makinasina test surumu gonderir
        yarn publish:prod
            ExpCrmAPI makinasina prod surumu gonderir

Derleme sirasi:
    Test icin:
        yarn build:test & yarn publish:test
    Prod icin:
        yarn build:prod & yarn publish:prod

Yayinlama:
    Backend:
        Test:
            ExpCrmApi makinasinda test dizinine kopyalanir.
            pm2 start npm --name "test" -- run start-test
            
        Prod:
            ExpCrmApi makinasinda prod dizinine kopyalanir.
            pm2 start npm --name "prod" -- run start-prod



/// Routing

/apps/satis                 => Satis dasboard, satis personel ana sayfa
/apps/satis/yonetim         => Satis Yonetim Dashboard

/apps/satis/musteriler      => Satis Musteri Listesi
/apps/satis/musteri/:id     => Musteri detay

/apps/satis/urunler
/apps/satis/urun/:id

/apps/satis/kisiler
/apps/satis/kisi/:musteriId/:id
